/*
Edited by Alfredo J. Velasco 8/12/2015 
For project: DRAM-ECC PCM-Data
Description: 
In this heuristic, we implement INDIRECT updates, where we only update bits that are reset after th update, so both cells that stay at reset state and cells that change from set to reset (0->0 and 1->0), and allow these reset cells to program the cells that have to be set (0->1) through thermal crosstalk. We also avoid programming cells that stay at 1 (1->1) because they do not experience any transient faults whatsoever. 
 
Inputs: [PATHtoDRAMtraceFile] [NumberOfBitsToProtect]
Outputs: Message specifying different figures of merit related to the heuristic

-----------------------------------Sample output-----------------------------------------------
Total number of programmed  cells : 
Total number of unprogrammed cells: 
Programming reduction percentage: 
*/

#include "processTraces.h"

//--------------------Global Vars---------------------------------------
std::ifstream DRAMtrace; 
unsigned long long addr1; 
unsigned long long addr2; 
long int wordCnt=0; 
long int eccWordCnt=0;
bool isFirstPair;
long int eccCnt=0;

int main(int argc, char* argv[]){
 
  DRAMtrace.open(argv[1]);
  int maxEccProtection = atoi(argv[2]);
  unsigned long long *dataPair[2];//= new unsigned long long*[2]; 
  unsigned long long data1[8];//= new unsigned long long[8];  
  unsigned long long data2[8];//= new unsigned long long[8];
  dataPair[0]=data1;
  dataPair[1]=data2;

  //Counting and avaraging variables. 
  unsigned long pairCnt=0;
  double unprogToProgRatio;
  const int bitBlockSize=512;
  long long unProgramed1s=0;
  long long totalStandardUps=0;
  int onesAfterUp=0;
  int zeroToOneUps=0;
  int newDisturb=0;
  int neededEccprotection;
  double progReduction=0;
  int errorScoreBoard[bitBlockSize]={0};
  unsigned long long prevAddr=0;
  unsigned long long curAddr=0;
  unsigned long totInverseSelecUp=0;
  unsigned long curIndirectUpCnt=0;
  unsigned long unsIndirectUpsCnt=0;
  unsigned long successIndirectUps=0;
  unsigned newIndirectProgCells=0;
  unsigned long totIndirectProgCells=0;
  unsigned long invSelectivePrograms=0;
  unsigned long totPartUps=0;
  unsigned long totIndirectErrors=0;
  
  //Get a pair of data to compare
  while(getDataPair(dataPair,pairCnt)){
     //Check the address of the update and reset dstScoreBoard can be reset when a new address is updated
    prevAddr = curAddr;
    curAddr = addr1; 
    if(curAddr!=prevAddr){
      resetScoreB(errorScoreBoard,bitBlockSize);
    }

    curIndirectUpCnt=0;

    //Tally cells that stay set across the update and the cells that can be indirectly programmed
    totPartUps+=cntPartiallyUpdatedBits(dataPair);
    unProgramed1s+=tallyOnes(dataPair);
    newIndirectProgCells=tallyPossIndirectUps(dataPair);
    totIndirectProgCells+=newIndirectProgCells;    
    pairCnt++;
    
    //Count the number of indirect errors
    totIndirectErrors+=tallyIndirectErrors(dataPair,errorScoreBoard);

    //Count the number of indirect errors at the current address. 
    neededEccprotection=tallyAddressIndErrors(dataPair[1],errorScoreBoard,bitBlockSize);
    
    if(neededEccprotection > maxEccProtection){
      //We have to do an inverse selective update
      resetScoreB(errorScoreBoard,bitBlockSize);
      totInverseSelecUp++;
      //We use a selective update that knows exactly where the errors are due to thermal crosstalk:
      invSelectivePrograms+=neededEccprotection;
      if(curIndirectUpCnt==0){
	unsIndirectUpsCnt++;
      }
    }
    else{
      curIndirectUpCnt++;
      successIndirectUps++;
    }
  }
  totalStandardUps=pairCnt*bitBlockSize; 
  progReduction= (double) totalStandardUps/(unProgramed1s); 
  
  printf("Total number of programmed  cells with heuristic 4: %lu\n", (pairCnt*bitBlockSize+invSelectivePrograms-(totPartUps+totIndirectProgCells)));
  printf("Total number of indirectly programmed cells: %lu\n",totIndirectProgCells);
  printf("Total number of indirect errors: %lu\n",totIndirectErrors);
  printf("Total number of successful indirect updates   : %lu\n",successIndirectUps);
  printf("Total number of unsuccessfull indirect updates: %lu\n",unsIndirectUpsCnt);
  printf("Total number of natural fixes: %lu\n",naturalFixCnt);
  printf("Reduced cell programs by: %f times\n",progReduction);

  return 0; 
}
