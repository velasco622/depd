/*
Edited by Alfredo J. Velasco 7/25/2015 
For project: DRAM-ECC PCM-Data
Description: This file contaiins header definitions for the logic in SortTraceFuns.cpp as well 
as include statements, global variables and structures.

*/

#include <fstream>
#include <iostream>
#include <map>
#include "CacheSim.h"


//Copy the digits of the address from string to addl 
void copyDigits(std::string line, char* addrStr); 

//Skips the data section of a trace entry to place the file position at the beginning of the next address
void skipToNextAddr(std::ifstream& file);

//Get data from the unsorted file and return a pointer to it
long long unsigned int* getData(std::ifstream& file);

//Print the histogram of address accesses and data to a file for later processing. 
void writeDRAMcurHist(std::map<long long int, DRAMupdate*>& DRAMhist, std::ofstream& file); 

//Function updates a map of all the updates to the DRAM address to print them out later in order
void updateDRAMcurHist(long long unsigned int addr,long long unsigned int* data, std::map<long long int, DRAMupdate*>& DRAMhist);

//Function cleans up the map to use it again
std::map<long long int, DRAMupdate*> cleanUpMap(std::map<long long int, DRAMupdate*>& DRAMhist); 

//function to print data to ofstream file
void fprintDataS(std::ofstream& file,long long unsigned int* data); 
