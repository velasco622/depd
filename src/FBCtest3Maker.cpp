/*
Edited by Alfredo J. Velasco 7/19/2015 
For project: DRAM-ECC PCM-Data
Description: Test3 for FlipBitCounter.cpp. This file creates a dummy test file with a single 
address at the top and random data being written every 8 bytes.   
Program flow: Create file, write the top address and data headers and then write all the data
Inputs: [NumWritesToMake] [seed]
Outpus: Dummy file at this path: 
"/home/alfredo/Documents/repos/depd/resrcs/testFiles/FlipBitCntTest3.txt" 
with the name number of input writes with the following format:
 
Address: 0xdeadbeef
Data: 
[WRITE]
\n
[WRITE]
\n
.
.
.
[WRITE]
 */
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <time.h>

int main(int argc, char* argv[]){
  //Open file and write header
  FILE* outFile; 
  outFile = fopen("/home/alfredo/Documents/repos/depd/resrcs/testFiles/FlipBitCntTest3.txt", "w"); 
  fprintf(outFile, "Address 0xdeadbeef\nData:\n"); 
  
  //Definitions for random number generation
  unsigned long long int dataWord; //This is 8 bytes
  long long int halfWordAvg; 
  unsigned int halfWord1; //This is 4 bytes
  unsigned int halfWord2; 
  //int seed = atoi(argv[2]); 
  srand(time(NULL)); 
  
  //Write the data 
  int numWrites = 0; 
  int maxWrites = atoi(argv[1]); 
  
  //Print 64 bytes of data separated with a new line character until numWrites>=maxWrites
  while(1){
    for(int i=0;i<8;i++){
      //Make 4 bytes of random numbers at a time
      halfWord1 = rand() % (long)pow(2,sizeof(int)*8); 
      halfWord2 = rand() % (long)pow(2,sizeof(int)*8);
      halfWordAvg+=halfWord1+halfWord2; 
      //Make the whole 8 bytes
      dataWord = halfWord1; 
      dataWord <<= (sizeof(int)*8); 
      dataWord |=  (long long)halfWord2;
      fprintf(outFile,"%#llx\n",dataWord); 
    }
    numWrites++;
    if(numWrites<maxWrites){
      fprintf(outFile,"\n");
    }
    else{
      printf("This is the avarage of the random values: %llu\n", halfWordAvg/numWrites); 
      break; 
    }
  }
  fclose(outFile); 
  return 0; 
}
