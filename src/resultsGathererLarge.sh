#!/bin/bash

#Edited by Alfredo J. Velasco 8/3/15
#Project: DEPD
#Script Description: Commands to produce the metrics/figures of merit for DEPD project

for i in $( ls /usr/xtmp/alfredo/pintoolResults/sorted_*_large.txt ); do 
    echo -----------------------Benchmark File: $i ---------------------------------
    echo 'Number of '
    echo '   Addresses with updates >= 2 (silent writes+valid updates):' 
    ups2orMore=`awk '{total=total+$1}END{printf total}' $i`
    echo '   '$ups2orMore

    echo '   Addresses with valid updates (more than one non-silent update)'
    validUps=`wc -l $i | cut -f1 -d' '`
    echo '   '$validUps

    echo '   Silent Writes:'
    silentWrites=$[ups2orMore-validUps]
    echo '   '$silentWrites
done
