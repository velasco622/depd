#!/bin/bash                                                                                                                
#Edited by Alfredo J. Velasco 8/4/15                                                                                       
#Project: DEPD                                                                                                             
#Script Description: Commands to produce the metrics/figures of merit for DEPD project                                     
#specifically, ratios of flipped bits. 

for i in $( ls /usr/xtmp/alfredo/pintoolResults/sorted_*_native.txt ); do
    echo -----------------------Benchmark File: $i ---------------------------------
    ./NewFlipBitsCounterWordGran $i
    echo ---------------------------------------------------------------------------
done
