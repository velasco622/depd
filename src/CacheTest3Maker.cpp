/*
Edited by Alfredo J. Velasco 7/19/2015 
For project: DRAM-ECC PCM-Data
Description: Test the cache associativity by writing to the same set over and over with numWays
different tags. Then, write with numWays+1 different tags and start seeing all the misses. 
Program flow: Write to the same set in a loop 100 times with the same tags from 0 to numWays-1. 
Then make another 100 writes with from 0 to numWays different tags. 
Inputs: [size] [ways] [setToHammerOn] [HammeringRepetitions] 
Outputs: Dummy file at this path: 
"/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest2.txt" 
with the name number of input writes with the following format:
 
R
[addr]
[aligned Addr]
\n
W
[addr]
[aligned addr]
\n
.
.
.
R
[addr]
[aligned addr] (no new line character after the last line of addr or data)
 */

#include "CacheTests.h"

int main(int argc, char* argv[]){
  //Set up
  FILE* outFile; 
  outFile = fopen("/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest3.txt", "w");
  int size = atoi(argv[1]); 
  int ways = atoi(argv[2]); 
  int set  = atoi(argv[3]);
  int reps = atoi(argv[4]);

  pWritesToSet(outFile,  size, ways, set, reps);
  pWritesP1ToSet(outFile, size, ways, set, reps);
  
  return 0; 
}
