/*
Edited by Alfredo J. Velasco 7/13/2015 
For project: DRAM-ECC PCM-Data
Description: This file contains the logic to process a trace of addresses and 
data writen to virtual memory by a single process. The addresses and data are
simulated to pass through a simple cache with [Whatever eviction you end up 
using] eviction and user input size and number of ways. The cache line size is
64 bytes through out. Different levels of cache can be simulated by creating
more Cache structs and cascading them. The top levels will evict to their 
respective lower levels until the lowest level cache evicts out to the memory
histogram i.e. DRAM.  
Simulation flow: A memory access is read from the trace input file
and placed within the cache. Data is
updated in the cache block if it is a writing operation, otherwise data is 
kept null. Whenever a block is evicted due to a capacity conflict, the block
is checked if it's dirty or clean and is writen back if dirty. The write backs
are recorded in an address histogram stored in an unordered map. The address
histogram contains all the writes that would be made to DRAM, and is writen out
to a file called DRAMtrace.out
Inputs: [cacheSize] [cacheWays] [/resrcs/DEPDtrace[Benchmark_InputSize].out] [pathToOutputFile]
Outpus: [/resrcs/DRAMtrace[Benchmark_InputSize].out]Trace with data and addresses for write backs to DRAM from the cache in
the order they are made. 
 */
#include <string>
#include <math.h>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <map>
using namespace std; 
//--------------------Structs and classes---------------------------------

//Defines a cache block to go inside a cache
class CacheBlock {
public:
  bool isOpen; 
  bool isDirty; 
  long long unsigned int addr; //Full effective address 
  long long unsigned int tag; 
  long long int* data; 
  CacheBlock(); 
}; 
CacheBlock::CacheBlock(){
  isOpen=true; 
  isDirty=false; 
  data = new long long[8]; 
}

//Defines a cache that can be different levels and different sizes. 
struct Cache {
  int level; 
  int numWays; 
  long int size; 
  CacheBlock** cacheBlocks; //2D array. 
}; 

//Defines an update to DRAM from the cache
class DRAMupdate{
public:
  DRAMupdate* nextUpdate; 
  DRAMupdate* lastUpdate; 
  long long int* data; 
  DRAMupdate(); 
};
DRAMupdate::DRAMupdate(){
  data = new long long[8];
}

//Defines an instruction taken from DEPDtrace***.out, a virtual instruction
class VirtIns{
public:
  bool isWrite; 
  long long unsigned int addr; 
  long long int* data; 
  VirtIns(); 
};
VirtIns::VirtIns(){
  data=new long long[8]; 
}

//--------------------Global variables---------------------------------------
//Writes to DRAM histogram per address. Each address is the key and the value is a pointer to a DRAMupdate structure, which is the first element in the histogram list. 
map<long long int, DRAMupdate*> DRAMhist; 

//Pointer to virtual trace
ifstream virtTrace; 

//--------------------Helper functions----------------------
//--------------------Move to header files/other src files later?--------
/*
//Update an open block in the cache
void updateBlock(long long int opTag, VirtIns* op, CacheBlock* block){ 
  //Check if it's a write operation
  if(op->isWrite){
    //Read data from file
    op->data = getData();
    saveWop(opTag, op, block); //block is dirty
  }
  else saveRop(opTag, op, block); //block is whatever it was to begin with
}
*/

//This function initializes our cache's blocks
Cache* initCache(long int size, int ways){
  int i;
  int j; 
  int lines = (size/64)/ways;
  Cache* newCache = new Cache;
  CacheBlock** newCacheBlocks = new CacheBlock*[lines];  
  for(i=0; i<lines;i++){
    newCacheBlocks[i] = new CacheBlock[ways]; 
  }
  
  /*
  //Test the cache
  for(i=0;i<lines;i++){
    for(j=0;j<ways;j++){
      printf("This is cache block (%d,%d), address:%p:\n",i,j, (void*)&newCacheBlocks[i][j]); 
      printf("isDirty:%d, isOpen:%d,data:%p\n",newCacheBlocks[i][j].isDirty, newCacheBlocks[i][j].isOpen,(void*)newCacheBlocks[i][j].data); 
    }
  }
  */

  newCache->cacheBlocks = newCacheBlocks;
  newCache->numWays = ways; 
  newCache->size = size; 
  newCache->level = 1; //Change this to user input later!!!
  return newCache; 
}

//Read an operation from the virtual trace to put it through the cache. 
bool getOp(VirtIns* op, ifstream* vTrace){
  string line; 
  //string::size_type sz = 0; 
  //Read the first line, i.e. R or W and check for file status
  if(getline(*vTrace, line)){
    //check if its a read
    if(line.compare("W")==0)
      op->isWrite=true; 
    else op->isWrite=false;
    //Read the second address and save it to op
    getline(*vTrace, line); //useless read
    getline(*vTrace, line); //Read with aligned address
    
    op->addr=(long long unsigned int)stoull(line,NULL,0);
    //Copy data if it's write
    if(op->isWrite){
      for(int i; i<8; i++){
	getline(*vTrace, line);
	if(line.compare("(nil)")==0)
	  op->data[i]=0; 
	else op->data[i] = stoull(line,NULL,0);
      }
    }
    getline(*vTrace, line);//useless read to throw out empty new line before next instruction
    //getline call above makes the next getline call pick up the next new instruction. 
    //cout<<line<<"\n"; 
    return true; 
  } 
  else{
    //vTrace is broken or we reached the end
    return false; 
  } 
}

//Systematically print out the attributes of a virtual instruction "op"
void printOp(VirtIns* op){
  printf("Printing an Op:\n"); 
  printf("op->isWrite: %d\n", op->isWrite); 
  printf("op->addr: %#llx\n", op->addr); 
  printf("op->data:\n");
  if(op->data){
    for(int i=0;i<8;i++){
      printf("%#llx\n",op->data[i]); 
    }
  }
  //We only add data to op when the write is writen back. 
  else printf("Data hasn't been added to op\n"); 
}

//Get the tag for the cache from the 64 byte aligned address in op
long long unsigned getTag(VirtIns* op, int size, int ways){
  long long unsigned tag = op->addr >> (int)(6+log2((size/64)/ways));
  //printf("this is address: %#llx\n", op->addr); 
  return tag; 
}

//Get the cache offset to index into the cache
long long unsigned getInx(VirtIns* op, int size, int ways){
  int blockOsetBits = 6;
  int addrSize = 64; //addSize is in bits, but This is also the size of our cache block conviniently in bytes
  long double cacheOsetBits = log2( (size/addrSize)/ways ); 
  //If the cache is 1 block in size, the index is always 1. 
  if(cacheOsetBits==0){
    return 0; 
  }
  int numTagBits = addrSize - (int)cacheOsetBits - blockOsetBits;
  //printf("This is numTagBits: %d\n", numTagBits);
  //printf("This is cacheOsetBits: %d\n",(int)cacheOsetBits); 
  long long unsigned inx = op->addr << numTagBits; 
  //printf("This is inx after we shift left by numTagBits: %#llx",inx); 
  inx = inx >> (numTagBits+blockOsetBits); 
  return inx; 
}

//Check if the block you need is in the cache
CacheBlock* checkForBlock(long long unsigned opTag, long long unsigned cacheInx, Cache* L1cache){
  CacheBlock* curBlock; 
  //compare tags with all the ways in the cache
  for(int i=0; i < L1cache->numWays; i++){
    //index into cache and way
    curBlock = &L1cache->cacheBlocks[cacheInx][i];
    //Check if block isOpen
    if(curBlock->isOpen){
      continue; 
    }
    else{
      //compare tags 
      if(curBlock->tag == opTag){
	return curBlock; 
      }
    }
  }
  return NULL; 
}

//Check for open ways in the cache line
CacheBlock* checkForOpenWays(long long unsigned opTag, long long unsigned cacheInx, Cache* L1cache){
  CacheBlock* curBlock; 
  for(int i=0; i < L1cache->numWays; i++){
    //index into cache and way
    curBlock = &L1cache->cacheBlocks[cacheInx][i];
    //Check if block isOpen
    if(curBlock->isOpen){
      return curBlock; 
    }
  }
  return NULL; 
}

//update the cache block with data and all that good stuff. 
void updateBlock(long long unsigned opTag, VirtIns* op, CacheBlock* block){
  //Check if read or write
  if(op->isWrite){
    block->isOpen = false; 
    block->isDirty= true; 
    block->addr   = op->addr; 
    block->tag    = opTag; 
    memcpy((void*)block->data, (void*)op->data, 64); 
  }
  else{
    block->isOpen = false; 
    block->addr   = op->addr;
    block->tag    = opTag; 
  }
}

//Find a block in the cache to evict, and return a pointer to it  
CacheBlock* getBlockToEvict(long long unsigned opCacheInx, Cache* L1cache){
  srand(time(NULL)); 
  int evictInx = rand() % L1cache->numWays; 
  return &L1cache->cacheBlocks[opCacheInx][evictInx]; 
}  


void updateDRAMhist(CacheBlock* block){
  //make the data update from cache block's attributes
  DRAMupdate* dUp = new DRAMupdate;
  //DRAMupdate testDup; 
  memcpy((void*)dUp->data,(void*)block->data,64);
  
  //Check if there's a value at the key address
  auto t = DRAMhist.find(block->addr); 
  if(t == DRAMhist.end()){  //testDup==NULL){
    dUp->nextUpdate = dUp; 
    dUp->lastUpdate = dUp; 
    DRAMhist[block->addr]=dUp; 
  }
  else{
    (t->second->lastUpdate)->nextUpdate = dUp; 
    t->second->lastUpdate=dUp; 
  }
}

//Write back the input block (This would be exteremely easy if the cache ran within pintool)
void cbWriteBack(CacheBlock* block){
  updateDRAMhist(block); 
  //Reset blocks attributes
  block->isOpen  = true;
  block->isDirty = false; 
  //block->data    = NULL; 
}

//Print data to an input file and end it with a new line character
void fprintData(FILE* file, long long* data){
  for(int i=0; i<8; i++){
    fprintf(file, "%#llx\n",data[i]); 
  }
  fprintf(file, "\n"); 
}


void writeDRAMhist(char* traceFileName){
  FILE* DRAMtrace; 
  DRAMupdate* curUp; 
  DRAMtrace = fopen(traceFileName, "w");
  //Iterate through keys
  for(auto const &entry : DRAMhist){
    fprintf(DRAMtrace, "Address: %#llx\n", entry.first); 
    fprintf(DRAMtrace, "Data: \n"); 
    curUp = entry.second; 
    //Edge case 1: linked list is 1 in size
    if(curUp==(entry.second)->lastUpdate){
      fprintData(DRAMtrace,curUp->data); 
    }
    //Edge case 2: Print first, middle and last elements in linked list
    else{
      //entry.second points to the beginning of a linked list of data updates
      //for each key, print out linked list of data updates to same key/address
      while(curUp != (entry.second)->lastUpdate){
	fprintData(DRAMtrace, curUp->data); 
	curUp = curUp->nextUpdate; 
      }
      //Print last last node in list that broke the while loop
      fprintData(DRAMtrace,curUp->data); 
    }
  }
  fclose(DRAMtrace); 
}


//remove memory allocated for the DRAMupdates
void cleanUpDRAMupdates(){
  DRAMupdate* cur; 
  DRAMupdate* next; 
  //Iterate through keys
  for(auto const &entry : DRAMhist){
    cur = entry.second;
    //Edge case 1: linked list is 1 in size
    if(cur==(entry.second)->lastUpdate){
      delete [] cur->data; 
      delete cur;  
    }
    //Edge case 2: Print first, middle and last elements in linked list
    else{
      //entry.second points to the beginning of a linked list of data updates
      //for each key, print out linked list of data updates to same key/address
      cur=entry.second->nextUpdate; 
      while(cur != (entry.second)->lastUpdate){
	//fprintData(DRAMtrace, curUp->data); 
	next = cur->nextUpdate; 
	delete [] cur->data; 
	delete cur;
	cur = next; 
      }
      //Print last last node in list that broke the while loop
      //fprintData(DRAMtrace,curUp->data); 
      delete [] entry.second->data; 
      delete entry.second; 
      delete [] cur->data;
      delete cur; 
    }
  }
} 

//Remove memory allocated for the cache.
void cleanUpCache(Cache* cache){
  int lines = (cache->size/64)/cache->numWays;
  for(int i=0; i< lines; i++){
    for(int j=0; j<cache->numWays; j++){
      delete [] (cache->cacheBlocks[i][j]).data; 
      //delete cache->cacheBlocks[i][j]; 
    }
    delete [] cache->cacheBlocks[i]; 
  }
  delete cache; 
} 


//-----------------------------------------------------------------------------
//--------------------Main function: Calls helper functions--------------------
//-----------------------------------------------------------------------------
int main(int argc, char* argv[]){
  //Local vars 
  bool isPresent;    //is block present in the cache?
  bool eofReached;   //to check if we got to the end of the virtual trace file
  long long unsigned int opTag; 
  long long unsigned int opCacheInx;
  struct Cache* L1cache; //Can make this an array of caches later
  int size= atoi(argv[1]); 
  int ways= atoi(argv[2]);
  virtTrace.open(argv[3]);
  char* DRAMtraceOutFile = argv[4]; 
  long long int* blockData;
  struct CacheBlock* block;
  struct VirtIns* op = new VirtIns;
 

  //initiallize the block structs in the cache
  L1cache = initCache(size, ways); //Change this name to just cache. We're only simulating one level. 
  
  //New operation/instruction from virtual trace
  while(getOp(op, &virtTrace)){ 
    //Get cache index and tag (to put the new memory operation into the cache)
    opTag = getTag(op, size, ways); 
    opCacheInx = getInx(op, size, ways); 
    //Check if block is already in cache 
    block = checkForBlock(opTag, opCacheInx, L1cache); 
    //block is present (block points to present block or NULL if not present.)
    if(block){
      updateBlock(opTag, op, block); 
    }
    //block not present
    else{
      //Check for open ways (in the indexed cache line)
      block = checkForOpenWays(opTag, opCacheInx, L1cache); 
      //Open ways (block points to an open block if there's an open way, otherwise block points to NULL)
      if(block){
	updateBlock(opTag, op, block); 
      }
      //no open ways (block = null)
      else{
	//Evict a block (to save new op into it)
	block = getBlockToEvict(opCacheInx, L1cache);//Returns pointer to an evicted block	
	//Check if block is dirty (so we can write it back if it is)
	if(block->isDirty){
	  cbWriteBack(block);
	}
	//Update the block with new update (regardless of block state)
	updateBlock(opTag, op, block);
      }
    } 
  }
  
  writeDRAMhist(DRAMtraceOutFile); 
  //Clean up memory
  cleanUpDRAMupdates(); 
  cleanUpCache(L1cache); 
  delete op; 

  return 0; 
}
