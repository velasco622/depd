/*
Edited by Alfredo J. Velasco 8/12/2015 
For project: DRAM-ECC PCM-Data
Description:In this heuristic, we implement STRONG updates, which allows us to get rid of ECC completely, as well as avoiding the programing of cells that stay set across updates. With STRONG updates we only program two sets of cells. 
     Set 1: The first set includes every cell that has to be in the reset state after the update, that is, cells that stay reset as well as cells that change from set (crystaline state) to reset. 
     Set 2: The second set includes cells that change from reset to set across the update. 
By only programming set 1 and 2, we can save programs on cells that are set intially and after the update, that is, cells that stay in the set state across the update. Since every reset cell is programmed with STRONG updates, we no longer need ECC for thermal crosstalk induced transient faults. 
Inputs: [PATHtoDRAMtraceFile] [NumberOfBitsToProtect]
Outpus: Message specifying different figures of merit related to the heuristic

-----------------------------------Sample output-----------------------------------------------
Total number of programmed  cells : 
Total number of unprogrammed cells: 
Programming reduction percentage: 
*/

#include "processTraces.h"

//--------------------Global Vars---------------------------------------
std::ifstream DRAMtrace; 
unsigned long long addr1; 
unsigned long long addr2; 
long int wordCnt=0; 
long int eccWordCnt=0;
bool isFirstPair;
long int eccCnt=0;

int main(int argc, char* argv[]){
 
  DRAMtrace.open(argv[1]);
  unsigned long long *dataPair[2];//= new unsigned long long*[2]; 
  unsigned long long data1[8];//= new unsigned long long[8];  
  unsigned long long data2[8];//= new unsigned long long[8];
  dataPair[0]=data1;
  dataPair[1]=data2;

  //Counting and avaraging variables. 
  unsigned long pairCnt=0;
  double unprogToProgRatio;
  const int bitBlockSize=512;
  long unProgramed1s=0;
  long totalStandardUps=0;
  double progReduction=0;

  //Get a pair of data to compare
  while(getDataPair(dataPair,pairCnt)){
    
    //Tally cells that stay set across the update
    unProgramed1s+=tallyOnes(dataPair);
    pairCnt++;
    
    //Get a new pair
  }
  totalStandardUps=pairCnt*bitBlockSize; 
  progReduction= (double) (unProgramed1s)/totalStandardUps; 
  
  printf("Total number of programmed  cells with heuristic 3: %lu\n", (totalStandardUps-unProgramed1s));
  printf("Total number of unprogrammed cells: %lu\n",unProgramed1s);
  printf("Programming reduction percentage: %f\n",progReduction*100);
  
  return 0; 
}
