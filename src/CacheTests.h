/*
Edited by Alfredo J. Velasco 7/19/2015 
For project: DRAM-ECC PCM-Data
Description: Writes a dummy file with memory requests that will exercise the 
cache's eviction logic to prove correct. Correct behavior involves not evicting
anything. That is, a properly working cache, will not evict anything when 
matched to the size and ways of the file produced here. 
Simulation flow: Make a series of write accesses to fill up all the ways in 
all the lines. Make write requests and then make read requests to 
the same addresses to show that everything stays in the cache. 
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

//--------------------------Function headers---------------------------------

//Function prints write operations that write in order, that is, go to an address, fill up the ways and go the next address
void pFillWrites(FILE* outFile,int size,int ways); 

//Reads in the same way as pFillWrites
void pOrdReads(FILE* outFile, int size, int ways); 

//Reads at an offset tag by log2(ways)
void pOffSetWrite(FILE* outFile, int size, int ways); 

//Write to same set over and over
void pWritesToSet(FILE* outFile, int size, int ways, int set, int reps);

//Write to same set over and over, but with one extra write than the number of ways
void pWritesP1ToSet(FILE*outFile, int size, int ways, int set, int reps); 

//print writes to half of the cache with ways going from ways to 2*ways-1
void pFillHalfWritesDifTag(FILE*outFile,int size,int ways);
