/*
Edited by Alfredo J. Velasco 7/19/2015 
For project: DRAM-ECC PCM-Data
Description: Make file with various memory requests. First wave of requetss are writes to every 
block in cache with tag incrementing from 0 to ways-1. Second wave only writes to half of the cache
locations with tag ways to 2*ways-1. The final wave are reads to every block with tag incrementing
from 0 to ways-1 to show half and half write misses. 
Inputs: size and ways of the cache to produce the test for
Outputs: Dummy file at this path: 
"/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest2_5.txt" 
with the name number of input writes with the following format:
 
R
[addr]
[aligned Addr]
\n
W
[addr]
[aligned addr]
\n
.
.
.
R
[addr]
[aligned addr] (no new line character after the last line of addr or data)
 */

#include "CacheTests.h"

int main(int argc, char* argv[]){
  //Set up
  FILE* outFile; 
  outFile = fopen("/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest2_5.txt", "w");
  int size = atoi(argv[1]); 
  int ways = atoi(argv[2]); 
 
  //wave of memory requests 1: print writes of 0's to every block to fill the cache
  pFillWrites(outFile,size,ways); 
  
  //wave 2: print writes to half of the cache with ways going from ways to 2*ways-1
  pFillHalfWritesDifTag(outFile,size,ways);

  //wave 3: read all the blocks in the cache with tag going from 0 to ways-1
  pOrdReads(outFile,size,ways); 

  return 0; 

}
