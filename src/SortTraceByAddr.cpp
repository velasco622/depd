/*
Edited by Alfredo J. Velasco 7/24/2015 
For project: DRAM-ECC PCM-Data
Description: This file takes an unsorted memory trace file with memory addresses and data at 
those addresses as input and sorts the data updates under their corresponding addresses. 
Program Flow: Whenever a new address (unsorted address) 'uAddr' is read, it's placed in a map of 
read addresses and into the curAddrSort map. A DRAMupdate is created and data below 'uAddr' is added to the DRAMupdate and the DRAMupdate is placed into the linked list starting at the value curAddrSort[uAddr]. This comparison is repeated for every address in the unsorted file. When every address is compared to uAddr, curAddSort[uAddr] linked list is printed out to a file, and everything is reset, uAddr is a new unsorted address, and is compared to the rest of addresses below it. This is done until uAddr has ben set to every address in the unsorted file. The output file is the sorted addresses by address with all the data updates to that address below each address. 
Inputs: [PathToUnsortedFile] [PathToSortedFile]
Output: [sortedTraceFileAtArgv[2]] 
*/

#include "SortTraceByAddr.h"

//---------------Required constant definitions to use CacheSimFuns--------------------------------
std::map<long long int, DRAMupdate*> DRAMhist; 
unsigned long long int lineCnt=0; 
int memOps=0; 
int writeBacks=0; 
int writeMisses=0;
int readMisses=0;
Cache* cache; 
char DRAMtraceOutFile[100] = "DRAMtraceFilePintoolTest.txt";
//------------------------------------------------------------------------------------------------

int main(int argc, char* argv[]){
  //set up the input and output files
  char* unsortedFilePath = argv[1]; 
  char* sortedFilePath = argv[2];
  std::ifstream unsFile(unsortedFilePath);
  std::ofstream sorFile(sortedFilePath);
  char addrStr[20]; 
  long long unsigned int addr; 
  long long unsigned int naddr; 
  std::string line; 
  std::vector<std::string> splitLine;
  std::map<long long int,int> doneAddrs;
  std::map<long long int, DRAMupdate*> curAddrHist;
  std::streampos addrSweepChkpt;
  long long unsigned int* data; 
  
  //Address sweep to build a curAddrHist map for each unique address
  while( std::getline(unsFile,line) ){
    copyDigits(line,addrStr);
    addr = std::stoull(addrStr,NULL,16);
    //Check if addr's writes have been sorted and writen(i.e. is in the doneAddr map)
    if(doneAddrs.count(addr)){
      skipToNextAddr(unsFile);
      continue; 
    }
    //Addr is new (gather writes to it and write them to file)
    else{
      //Take unsFile position checkpoint (to continue the sweep from next address)
      addrSweepChkpt = unsFile.tellg();
      doneAddrs[addr]=1;
      data = getData(unsFile);
      updateDRAMcurHist(addr,data,curAddrHist);
      //Comparison sweep here: 
      while(getline(unsFile,line)){
	copyDigits(line,addrStr);
	naddr = std::stoull(addrStr,NULL,16);
	if(naddr!=addr){
	  skipToNextAddr(unsFile);
	  continue;
	}
	else{
	  data = getData(unsFile);
	  updateDRAMcurHist(addr,data,curAddrHist);
	}
      }
      //Write curAddrHist to output file and reset it to reuse
      writeDRAMcurHist(curAddrHist,sorFile);
      curAddrHist=cleanUpMap(curAddrHist);
      unsFile.clear();
      unsFile.seekg(addrSweepChkpt);
      skipToNextAddr(unsFile);
    }
  }
  return 0; 
}
