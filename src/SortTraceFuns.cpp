/*
Edited by Alfredo J. Velasco 7/25/2015 
For project: DRAM-ECC PCM-Data
Description: Functions used in SortTraceByAddr.cpp mainly. 

*/


#include "SortTraceByAddr.h"

//Copy the digits of the address from line to addr
void copyDigits(std::string line, char* addrStr){
  int numStarts=8;
  int endOfLineIndex;
  endOfLineIndex=line.size()/sizeof(char);
  for(int i=numStarts;i<endOfLineIndex;i++){
    addrStr[i-numStarts]=line[i];
  }
  addrStr[endOfLineIndex]='\0'; 
}

//Skips the data section of a trace entry to place the file position at the beginning of the next address
void skipToNextAddr(std::ifstream& file){
  std::string line;
  int numLinesBetAddrs = 10; 
  for(int i=0;i<numLinesBetAddrs;i++){
    std::getline(file,line);
  }
  //int bytesBetAddrs = 75; //Number of bytes between addresses in the trace file 'file'
  //std::streampos nextAddrPos = bytesBetAddrs+file.tellg()+1;
  //file.seekg(nextAddrPos);
}

//Get data from the unsorted file and return a pointer to it
long long unsigned int* getData(std::ifstream& file){
  std::string line;
  std::getline(file,line);//Read the line that says "Data" to start copying real data
  static long long unsigned int data[8]; 
  for(int i=0;i<8;i++){
    getline(file,line);
    data[i]=stoull(line,NULL,16);
  }
  getline(file,line);//Read empty line to start at next address or EOF. 
  return data; 
}

//Function updates a map of all the updates to the DRAM address to print them out later in order
void updateDRAMcurHist(long long unsigned int addr,long long unsigned int* data, std::map<long long int, DRAMupdate*>& DRAMhist){
  //make the data update from cache block's attributes
  DRAMupdate* dUp = new DRAMupdate;
  dUp->data = new long long unsigned int[8]; 
  memcpy((void*)dUp->data,(void*)data,64);
  
  //Check if there's a value at the key address
  auto t = DRAMhist.find(addr); 
  if(t == DRAMhist.end()){  //testDup==NULL){
    dUp->nextUpdate = dUp; 
    dUp->lastUpdate = dUp; 
    DRAMhist[addr]=dUp; 
  }
  else{
    (t->second->lastUpdate)->nextUpdate = dUp; 
    t->second->lastUpdate=dUp; 
  }
}

//Function cleans up the map to use it again
std::map<long long int, DRAMupdate*> cleanUpMap(std::map<long long int, DRAMupdate*>& DRAMhist){
  DRAMupdate* cur; 
  DRAMupdate* next; 
  //Iterate through keys
  for(auto const &entry : DRAMhist){
    cur = entry.second;
    //Edge case 1: linked list is 1 in size
    if(cur==(entry.second)->lastUpdate){
      delete [] cur->data; 
      delete cur;  
    }
    //Edge case 2: Print first, middle and last elements in linked list
    else{
      //entry.second points to the beginning of a linked list of data updates
      //for each key, print out linked list of data updates to same key/address
      cur=entry.second->nextUpdate; 
      while(cur != (entry.second)->lastUpdate){
	//fprintData(DRAMtrace, curUp->data); 
	next = cur->nextUpdate; 
	delete [] cur->data; 
	delete cur;
	cur = next; 
      }
      //Print last last node in list that broke the while loop
      //fprintData(DRAMtrace,curUp->data); 
      delete [] entry.second->data; 
      delete entry.second; 
      delete [] cur->data;
      delete cur; 
    }
  }
  DRAMhist.clear(); 
  return DRAMhist;
}

//function to print data to ofstream file
void fprintDataS(std::ofstream& file,long long unsigned int* data){
  int dataChunks = 8; //8 data chunks, of size 8 bytes each to make up the 64 byte cache line
  file << std::hex << std::showbase ;
  for(int i=0; i<dataChunks; i++){
    file << data[i] << std::endl;
  }  
  //fprintf(file, "\n"); 
  file << std::endl;
}



//Print the histogram of address accesses and data to a file for later processing. 
void writeDRAMcurHist(std::map<long long int, DRAMupdate*>& DRAMhist, std::ofstream& file){
  DRAMupdate* curUp; 
  file << std::hex << std::showbase ;
  //Iterate through keys
  for(auto const &entry : DRAMhist){
    //fprintf(DRAMtrace, "Address: %#llx\n", entry.first); 
    //fprintf(DRAMtrace, "Data: \n"); 
    curUp = entry.second; 
    //Edge case 1: linked list is 1 in size
    if(curUp==(entry.second)->lastUpdate){
      continue;
      //fprintDataS(file,curUp->data); 
    }
    //Edge case 2: Print first, middle and last elements in linked list
    else{
      //entry.second points to the beginning of a linked list of data updates
      //for each key, print out linked list of data updates to same key/address
      file << "Address: " << entry.first << std::endl;
      file << "Data:\n" << std::endl;
      while(curUp != (entry.second)->lastUpdate){
	fprintDataS(file, curUp->data); 
	curUp = curUp->nextUpdate; 
      }
      //Print last last node in list that broke the while loop
      fprintDataS(file,curUp->data); 
    }
  }
}
