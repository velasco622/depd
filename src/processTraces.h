/*
Edited by Alfredo J. Velasco 8/10/2015 
For project: DRAM-ECC PCM-Data (DEPD)
Description: This h file contains the headers and global variables for the functions in 
processTraceFuns.cpp and the heuristics*.cpp files. 
*/
#include <math.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <iostream>
#include <vector>
#include <sstream>

//Global variables. Put in .h file!
#define LEN(x)  (sizeof(x) / sizeof((x)[0]))
extern long int eccCnt;//=0;
//---------------------Global Vars--------------------------------------                                          
extern std::ifstream DRAMtrace;
extern unsigned long long addr1;
extern unsigned long long addr2;
extern long int wordCnt;//=0;
extern long int eccWordCnt;//=0;
extern long int naturalFixCnt;
//bool isFirstPair;

//Split a string into a supplied vector. Inspired by:                                                             
//http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);

//Get a pair of data from the DRAMtrace                                                                           
bool getDataPair(unsigned long long** dataPair, int pairCnt);

//Get ECC bits from the input data word of size 64 bits.
int getWordECC(unsigned long long data);

//Get Average bitflips between two pairs of data at the block granularity
float getAvgDfsBG(unsigned long long** pair);

//Get Average bitflips between two pairs of ECC at the block granularity
float getAvgECCfsBG(int** pair);

//Get Average bitflips between two pairs of data at the word granularity
float getAvgDfsWG(unsigned long long** pair);

//Get Average bitflips between two pairs of ECC at the word granularity
float getAvgECCfsWG(int** pair);

//Use to set every element in the input array to 0's               
void resetScoreB(int* dstScoreBoard,int);

//Counts the number of bits that could be partially updated,       
//That is, the number of bits that were the same across the update 
int cntPartiallyUpdatedBits(unsigned long long** dataPair);


//Find the number of disturbed bits in the update                  
int tallyUpdateDisturb(unsigned long long** dataPair,int* dstScoreBoard);

//Find the number of disturbed bits currently at the current address. Can be used with any heuristic. This function's name should really be tallyAddressNeededEccProtection() or tallyAddressErrorsToFixWithEcc()...
int tallyAddressDisturb(unsigned long long int*, int* dstScoreBoard, int );

//Find the number of 1's in the input array of data. These are the cells that do not have to be programmed because a selective update only updates 0's. 
int tallySelecCompProgSaved(unsigned long long int*);
 
//Find the number of set cells that stay set across the update, (1's that are 1's in both pairs of data).   
int tallyOnes(unsigned long long** dataPair);

  
//Find the number of indirect errors in the dataPair update. An indirect error occurs when a cell transitions from 0->1 and there is no cell around it that is a 0 after th update. 
int tallyIndirectErrors(unsigned long long** dataPair,int* errorScoreBoard);


//Find the number of possible indirect updates. These are cells being updated from 0->1 that also have at least one 0 next to them after the update. For example, if we had two cells, cell a and cell b, where a goes from 0->1 and b goes from 0->0 cell a is a possible indirect update, because by going from 0->0, b would set cell a through thermal crosstalk. 
int tallyPossIndirectUps(unsigned long long** dataPair);

//Find the number of indirect errors at this address from the score board and the data. We need the data because there may be natural fixes in the data that the scoreboard does not reflect.
int tallyAddressIndErrors(unsigned long long int* newData, int* dstScoreBoard, int boardSize);
 
