/*
Edited by Alfredo J. Velasco 7/20/2015 
For project: DRAM-ECC PCM-Data
Description: This file will post as the pintool. The functions from CacheSimFuns.cpp are called 
here to ensure propper integration, .h file names and correct behaviour. The logic within the
CacheSimFuns.cpp functions has already been tested with CacheTests 1 through 4. 
*/

#include "CacheSim.h" 
//----------------------------------------------------------------------------
//------------------------------Global variables--(Place these in pintool)----
//----------------------------------------------------------------------------
//Writes to DRAM histogram per address. Each address is the key and the value is a pointer to a DRAMupdate structure, which is the first element in the histogram list.  
std::map<long long int, DRAMupdate*> DRAMhist; 

//Pointer to virtual trace
std::ifstream virtTrace; 

unsigned long long int lineCnt=0; 
char DRAMtraceOutFile[100] = "DRAMtraceFilePintoolTest.txt"; //Rename this in pintool
int memOps=0; 
int writeBacks=0; 
int writeMisses=0;
int readMisses=0;
Cache* cache; 


//----------------------------------------------------------------------------
//------------------------------Helper Functions(This test only)--------------
//----------------------------------------------------------------------------
//Make an array of virtual instructions to the same set with different tags from 0 to 'ways'-1
VirtIns* makeWvInsts(int set, int ways, int blockSize, int numBlocks){
  struct VirtIns* ops = new struct VirtIns[ways]; 
  long long unsigned int addr = 0; 
  //Make operations
  for(int i=0;i<ways;i++){
    //ops[i] = new VirtIns; 
    ops[i].isWrite = true; 
    ops[i].addr = addr; 
    addr+= ( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ); 
  }
  return ops; 
}

//Make an array of virtual instructions to the same set with different tags from 0 to 'ways'-1
VirtIns* makeWvInstsDifTag(int set, int ways, int blockSize, int numBlocks){
  struct VirtIns* ops = new struct VirtIns[ways]; 
  long long unsigned int addr = ways*( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ); 
  //Make operations
  for(int i=0;i<ways;i++){
    //ops[i] = new VirtIns; 
    ops[i].isWrite = false; 
    ops[i].addr = addr; 
    addr+= ( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ); 
  }
  return ops; 
}

int main(int argc, char* argv[]){

  //-------------------------PinToolOpeningCall_calls-------------------------------
  //--------------Call this in the opening call of pintool-------------------------
  //-------------------------------------------------------------------------------
  //Initial Set up
  int ways = 8; 
  int blockSize=64;     //cache block size in bytes
  int size = pow(2,22); //2^22=4MB cache size in bytes
  int sets = (size/blockSize)/ways;
  long long unsigned int* addr; //To save output of the accessCacheSim
  struct VirtIns* ops; 
  
  //Make cache
  cache = initCache(size, ways); 
  

  //-------------------------PinToolCallbacks_calls--------------------------------
  //--------------------Call this in the pintool callbacks-------------------------
  //-------------------------------------------------------------------------------
  //put ops in cache
  ops = makeWvInsts(0,ways, blockSize,sets); 
  for(int i=0;i<ways;i++){
    addr = accessCacheSim(&ops[i],cache);
    //check addr
    if(addr){
      //Addr is not null, therefore we evicted addr
      printf("Address evicted: %llx\n\n",*addr);
      //Get data from PIN_SafeCopy() (dummy data here for now)
      long long unsigned int data[8]; 
      for(int j=0;j<8;j++){
	data[j]=0; 
      }
      //Update DRAMhist
      updateDRAMhist(*addr,data);
    }
    else printf("Address %llx stayed in cache\n\n",ops[i].addr); 
  }
  
  //Put ops into same set with different tags
  ops = makeWvInstsDifTag(0,ways,blockSize,sets);
  for(int i=0;i<ways;i++){
    addr = accessCacheSim(&ops[i],cache);
    //check addr
    if(addr){
      //On Eviction
      printf("Address evicted: %llx\n\n",*addr);
      //Get data from PIN_SafeCopy() (dummy data here for now)
      long long unsigned int data[8]; 
      for(int j=0;j<8;j++){
	data[j]=0; 
      }
      //Update DRAMhist
      updateDRAMhist(*addr,data);  
    }
    else printf("Address %llx stayed in cache\n\n",ops[i].addr); 
  }
  
  //----------------------PintoolClosingCalls_Call---------------------------------
  //--------------Call this in the closing call of pintool-------------------------
  //-------------------------------------------------------------------------------
  writeDRAMhist(DRAMtraceOutFile); 
  
  return 0; 
}
