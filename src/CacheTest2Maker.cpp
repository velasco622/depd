/*
Edited by Alfredo J. Velasco 7/19/2015 
For project: DRAM-ECC PCM-Data
Description: Test the cache filling up and evicting every thing that went into it correctly
Program flow: Write memory operations in a file to fill up the cache with cache misses. Make anoth
er set of writes to the same addresses but with different tags to miss in the cache every write.
Inputs: size and ways of the cache to produce the test for
Outputs: Dummy file at this path: 
"/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest2.txt" 
with the name number of input writes with the following format:
 
R
[addr]
[aligned Addr]
\n
W
[addr]
[aligned addr]
\n
.
.
.
R
[addr]
[aligned addr] (no new line character after the last line of addr or data)
 */

#include "CacheTests.h"

int main(int argc, char* argv[]){
  //Set up
  FILE* outFile; 
  outFile = fopen("/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest2.txt", "w");
  int size = atoi(argv[1]); 
  int ways = atoi(argv[2]); 
 
  //print writes of 0's to every block to fill the cache
  pFillWrites(outFile,size,ways); 
  
  //Make offset read to evict all the writen blocks. 
  pOffSetWrite(outFile,size,ways); 
  
  //update the same addresses to make them dirty
  //pFillWrites(outFile,size,ways); 
  
  //Make read operations in order from lower to higher addresses, going through the tags to show there's no eviction. 
  //pOrdReads(outFile, size,ways); 
  
  fclose(outFile); 
  return 0; 
}
