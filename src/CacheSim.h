/*
Edited by Alfredo J. Velasco 7/20/2015 
For project: DRAM-ECC PCM-Data
Description: This file contains header definitions for the logic to run a cache simulator. 
It all works with VirtIns strucs that can be made from a trace file or from anything really. 
A function that makes the VirtIns objects can be made however, and the VirtIns can be put through 
the cache to get realistic evictions with LRU policy. 
Simulation flow: A memory access is read from somewhere and placed within the cache. Data is
kept null. Whenever a block is evicted due to a capacity conflict, the block
is checked if it's dirty or clean and is writen back if dirty. 

--------------------------------THIS MIGHT NOT BE TRUE AFTER COMBINING THIS TO PINTOOL-------------
The write backs are recorded in an address histogram stored in an unordered map. The address
histogram contains all the writes that would be made to DRAM, and is writen out
to a file called DRAMtrace.out
-------------------------------------------------------------------------------------------------
*/

#include <string>
#include <vector>
#include <math.h>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <map>
#include <algorithm>
//using namespace std; 

//------------------------------------------------------------------------
//--------------------Structs and classes---------------------------------
//------------------------------------------------------------------------


//Defines a cache block to go inside a cache
struct CacheBlock {
  //public:
  bool isValid; //Bit that is 1 when the data is in the cache, and 0 when not 
  bool isDirty; 
  long long unsigned int addr; //Full effective address 
  long long unsigned int tag; 
  //long long int* data; 
  //CacheBlock(); 
};
/* 
CacheBlock::CacheBlock(){
  isValid=false; 
  isDirty=false; 
  //data = new long long[8]; 
}
*/

//Defines a cache that can be different levels and different sizes. 
struct Cache {
  //int level; 
  int numWays; 
  int size; 
  CacheBlock** cacheBlocks; //2D array. 
}; 

//Defines an update to DRAM from the cache
struct DRAMupdate{
  //public:
  DRAMupdate* nextUpdate; 
  DRAMupdate* lastUpdate; 
  long long unsigned int* data; 
  //DRAMupdate(); 
};
/*
DRAMupdate::DRAMupdate(){
  data = new long long[8];
}
*/
//Defines an instruction taken from DEPDtrace***.out, a virtual instruction
struct VirtIns{
  bool isWrite; 
  long long unsigned int addr; 
};

//---------------------------------------------------------------------------
//--------------------Global variables---------------------------------------
//---------------------------------------------------------------------------
//Writes to DRAM histogram per address. Each address is the key and the value is a pointer to a DRAMupdate structure, which is the first element in the histogram list. 
extern std::map<long long int, DRAMupdate*> DRAMhist; 

//Pointer to virtual trace
extern std::ifstream virtTrace; 

//To count the lines we have read
extern unsigned long long int lineCnt;//=0; 
extern int memOps;//=0; 
extern int writeBacks;//=0; 
extern int writeMisses;//=0;
extern int readMisses;//=0;

//---------------------------------------------------------------------------
//----------------------------Function Headers-------------------------------
//---------------------------------------------------------------------------
//This function initializes our cache's blocks
Cache* initCache(long int size, int ways); 

//Read an operation from the virtual trace to put it through the cache. 
bool getOp(struct VirtIns* op, std::ifstream* vTrace); 

//Systematically print out the attributes of a virtual instruction "op"
void printOp(struct VirtIns* op); 

//Get the tag for the cache from the 64 byte aligned address in op
long long unsigned getTag(struct VirtIns* op, int size, int ways); 

//Get the cache offset to index into the cache
long long unsigned getInx(struct VirtIns* op, int size, int ways); 

//LRU is implemented by making the set a FIFO. The first block in the set is the most recently updated block, and the last one is the LRU block. updateBlock(), getBlockToEvict(), and checkForInvalidWays() are edited to set up this behavior. 
//Copy all the values and leave the source block invalid and clean
void copyBlocks(CacheBlock* srcBlock,CacheBlock* dstBlock);

//Check if the block you need is in the cache
CacheBlock* checkForBlock(long long unsigned opTag, long long unsigned cacheInx, struct Cache* L1cache); 

//Check for open ways in a set
CacheBlock* checkForInvalidWays(long long unsigned opTag, long long unsigned cacheInx, struct Cache* L1cache); 

//update the cache block and all that good stuff. 
void updateBlock(long long unsigned opTag, long long unsigned opCacheInx, struct VirtIns* op, CacheBlock* block, struct Cache* cache); 

//Find a block in the cache to evict, and return a pointer to it  
CacheBlock* getBlockToEvict(long long unsigned opCacheInx, struct Cache* L1cache); 

//Add writes to a linked list to keep our writes organized by address to compare the right writes
void updateDRAMhist(long long unsigned int addr/*CacheBlock* block*/,long long unsigned int* data); 
//Write back the input block (This allows us to print out the DRAM addresses and their writes in the order in which they are made)
void cbWriteBack(CacheBlock* block/*,long long unsigned int* data*/); 

//Print data to an input file and end it with a new line character
void fprintData(FILE* file, long long* data); 

//Print the histogram of address accesses and data to a file for later processing. 
void writeDRAMhist(char* traceFileName); 

//remove memory allocated for the DRAMupdates
void cleanUpDRAMupdates(); 

//Remove memory allocated for the cache.
void cleanUpCache(struct Cache* cache); 

//Access the cache
long long unsigned int* accessCacheSim(/*int size, int ways, */struct VirtIns* op, struct Cache* cache/*int argc, char* argv[]*/); 
