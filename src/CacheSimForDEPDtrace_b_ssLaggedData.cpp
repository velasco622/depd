/*
Edited by Alfredo J. Velasco 7/13/2015 
For project: DRAM-ECC PCM-Data
Description: (7/20/2015) ORIGINAL ATTEMP WITH WORKING FUNCTIONS WITH VIRTUAL TRACE FILE AS INPUT. THIS FILE IS SPLIT UP INTO FUNCTIONS TO USE WITHIN PINTOOL (DEPDtrace.cpp) IN FILES: CacheSim.h and CacheSimFuns.cpp. 
This file contains the logic to process a trace of addresses and 
data writen to virtual memory by a single process. The addresses and data are
simulated to pass through a simple cache with [Whatever eviction you end up 
using] eviction and user input size and number of ways. The cache line size is
64 bytes through out. Different levels of cache can be simulated by creating
more Cache structs and cascading them. The top levels will evict to their 
respective lower levels until the lowest level cache evicts out to the memory
histogram i.e. DRAM.  
Simulation flow: A memory access is read from the trace input file
and placed within the cache. Data is
updated in the cache block if it is a writing operation, otherwise data is 
kept null. Whenever a block is evicted due to a capacity conflict, the block
is checked if it's dirty or clean and is writen back if dirty. The write backs
are recorded in an address histogram stored in an unordered map. The address
histogram contains all the writes that would be made to DRAM, and is writen out
to a file called DRAMtrace.out
Inputs: [cacheSize] [cacheWays] [/resrcs/DEPDtrace[Benchmark_InputSize].out] path to trace with memory 
accesses made by the process as well as data for the writes. 
Outputs: [/resrcs/DRAMtrace[Benchmark_InputSize].out]Trace with data and addresses for write backs to DRAM from the cache in
the order they are made. 
 */
#include <string>
#include <math.h>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <map>
using namespace std; 
//------------------------------------------------------------------------
//--------------------Structs and classes---------------------------------
//------------------------------------------------------------------------

//Defines a cache block to go inside a cache
class CacheBlock {
public:
  bool isValid; //Bit that is 1 when the data is in the cache, and 0 when not 
  bool isDirty; 
  long long unsigned int addr; //Full effective address 
  long long unsigned int tag; 
  long long int* data; 
  CacheBlock(); 
}; 
CacheBlock::CacheBlock(){
  isValid=false; 
  isDirty=false; 
  data = new long long[8]; 
}

//Defines a cache that can be different levels and different sizes. 
struct Cache {
  int level; 
  int numWays; 
  int size; 
  CacheBlock** cacheBlocks; //2D array. 
}; 

//Defines an update to DRAM from the cache
class DRAMupdate{
public:
  DRAMupdate* nextUpdate; 
  DRAMupdate* lastUpdate; 
  long long int* data; 
  DRAMupdate(); 
};
DRAMupdate::DRAMupdate(){
  data = new long long[8];
}

//Defines an instruction taken from DEPDtrace***.out, a virtual instruction
class VirtIns{
public:
  bool isWrite; 
  long long unsigned int addr; 
  long long int* data; 
  VirtIns(); 
};
VirtIns::VirtIns(){
  data=new long long[8]; 
}

//---------------------------------------------------------------------------
//--------------------Global variables---------------------------------------
//---------------------------------------------------------------------------
//Writes to DRAM histogram per address. Each address is the key and the value is a pointer to a DRAMupdate structure, which is the first element in the histogram list. 
map<long long int, DRAMupdate*> DRAMhist; 

//Pointer to virtual trace
ifstream virtTrace; 

//To count the lines we have read
unsigned long long int lineCnt=0; 
int memOps=0; 
int writeBacks=0; 
int writeMisses=0;
int readMisses=0;

//--------------------Helper functions----------------------------------------
//--------------------Move to header files/other src files later?-------------
//----------------------------------------------------------------------------
/*
//Update an open block in the cache
void updateBlock(long long int opTag, VirtIns* op, CacheBlock* block){ 
  //Check if it's a write operation
  if(op->isWrite){
    //Read data from file
    op->data = getData();
    saveWop(opTag, op, block); //block is dirty
  }
  else saveRop(opTag, op, block); //block is whatever it was to begin with
}
*/

//This function initializes our cache's blocks
Cache* initCache(long int size, int ways){
  //Create blocks
  int i;
  int j; 
  int sets = (size/64)/ways;
  Cache* newCache = new Cache;
  CacheBlock** newCacheBlocks = new CacheBlock*[sets];  
  for(i=0; i<sets;i++){
    newCacheBlocks[i] = new CacheBlock[ways]; 
  }
  
  /*
  //Test the cache
  for(i=0;i<sets;i++){
    for(j=0;j<ways;j++){
      printf("This is cache block (%d,%d), address:%p:\n",i,j, (void*)&newCacheBlocks[i][j]); 
      printf("isDirty:%d, isOpen:%d,data:%p\n",newCacheBlocks[i][j].isDirty, newCacheBlocks[i][j].isOpen,(void*)newCacheBlocks[i][j].data); 
    }
  }
  */
  //Point cache to those blocks
  newCache->cacheBlocks = newCacheBlocks;
  newCache->numWays = ways; 
  newCache->size = size; 
  newCache->level = 1; //Change this to user input later!!!
  return newCache; 
}

//Read an operation from the virtual trace to put it through the cache. 
bool getOp(VirtIns* op, ifstream* vTrace){
  string line; 
  //printf("Line Count: %llu",lineCnt); 
  //string::size_type sz = 0; 
  //Read the first line, i.e. R or W and check for file status
  if(getline(*vTrace, line)){
    lineCnt++; 
    //check if its a read
    if(line.compare("W")==0)
      op->isWrite=true; 
    else op->isWrite=false;
    memOps++; 
    //Read the second address and save it to op
    getline(*vTrace, line); //useless read
    getline(*vTrace, line); //Read's aligned address
    lineCnt++;
    lineCnt++;							
    if(line.compare("")==0) getline(*vTrace,line); //Inserted this for a weird extra new line at line 85,330,275 in ..../resrcs/DEPDtrace_blackscholes_simsmallLaggedData.out
    if(line.compare("")==0) return false; //At the end of the trace we get two "" characters in a row, so we can exit then. 
    op->addr = stoull(line,NULL,0);
    //Copy data if it's write
    if(op->isWrite){
      for(int i=0; i<8; i++){
	getline(*vTrace, line);
	lineCnt++;
	if(line.compare("(nil)")==0)
	  op->data[i]=0; 
	else op->data[i] = stoull(line,NULL,0);
      }
    }
    getline(*vTrace, line);//useless read to throw out empty new line before next instruction
    //getline call above makes the next getline call pick up the next new instruction. 
    //cout<<line<<"\n";  
    lineCnt++;
    return true; 
  } 
  else{
    //vTrace is broken or we reached the end
    return false; 
  } 
}

//Systematically print out the attributes of a virtual instruction "op"
void printOp(VirtIns* op){
  printf("Printing an Op:\n"); 
  printf("op->isWrite: %d\n", op->isWrite); 
  printf("op->addr: %#llx\n", op->addr); 
  printf("op->data:\n");
  if(op->data){
    for(int i=0;i<8;i++){
      printf("%#llx\n",op->data[i]); 
    }
  }
  //We only add data to op when the write is writen back. 
  else printf("Data hasn't been added to op\n"); 
}

//Get the tag for the cache from the 64 byte aligned address in op
long long unsigned getTag(VirtIns* op, int size, int ways){
  long long unsigned tag = op->addr >> (int)(6+log2((size/64)/ways));
  //printf("this is address: %#llx\n", op->addr); 
  return tag; 
}

//Get the cache offset to index into the cache
long long unsigned getInx(VirtIns* op, int size, int ways){
  int blockOsetBits = 6;
  int addrSize = 64; //addSize is in bits, but This is also the size of our cache block conviniently in bytes
  long double cacheOsetBits = log2( (size/addrSize)/ways ); 
  //If the cache is 1 block in size, the index is always 1. 
  if(cacheOsetBits==0){
    return 0; 
  }
  int numTagBits = addrSize - (int)cacheOsetBits - blockOsetBits;
  //printf("This is numTagBits: %d\n", numTagBits);
  //printf("This is cacheOsetBits: %d\n",(int)cacheOsetBits); 
  long long unsigned inx = op->addr << numTagBits; 
  //printf("This is inx after we shift left by numTagBits: %#llx",inx); 
  inx = inx >> (numTagBits+blockOsetBits); 
  return inx; 
}


//Copy all the values and leave the source block invalid and clean
void copyBlocks(CacheBlock* srcBlock,CacheBlock* dstBlock){
  dstBlock->isValid=srcBlock->isValid;
  dstBlock->isDirty=srcBlock->isDirty; 
  dstBlock->addr   =srcBlock->addr;  
  dstBlock->tag    =srcBlock->tag; 
  dstBlock->data   =srcBlock->data; 
  //Set up srcBlock to be invalid
  srcBlock->isValid=false; 
  srcBlock->isDirty=false; 
}

//Check if the block you need is in the cache
CacheBlock* checkForBlock(long long unsigned opTag, long long unsigned cacheInx, Cache* L1cache){
  int resBlockInx; 
  CacheBlock* curBlock;
  CacheBlock tempBlock;
  CacheBlock* lastBlock;
  CacheBlock* prevLastBlock;
  //compare tags with all the ways in the cache
  for(int i=0; i < L1cache->numWays; i++){
    //index into cache and way
    curBlock = &L1cache->cacheBlocks[cacheInx][i];
    //Check if block isValid
    if(!curBlock->isValid){
      continue; 
    }
    else{
      //compare tags 
      if(curBlock->tag == opTag){
	//Save resident block in temp
	copyBlocks(curBlock,&tempBlock); 
	resBlockInx = i; 
	//Cascade blocks' contents to fill the spot of the resident block
	for(int j=resBlockInx;j>0;j--){
	  lastBlock = &L1cache->cacheBlocks[cacheInx][j];
	  prevLastBlock = &L1cache->cacheBlocks[cacheInx][j-1];
	  copyBlocks(prevLastBlock,lastBlock);
	}
	//Return first block in set to be possibly writen back and updated
	curBlock = &L1cache->cacheBlocks[cacheInx][0];
	copyBlocks(&tempBlock,curBlock); 
	return curBlock; 
      }
    }
  }
  return NULL; 
}


//LRU is implemented by making the set a FIFO. The first block in the set is the most recently updated block, and the last one is the LRU block. updateBlock(), getBlockToEvict(), and checkForInvalidWays() are edited to set up this behavior. 
//Check for open ways in a set
CacheBlock* checkForInvalidWays(long long unsigned opTag, long long unsigned cacheInx, Cache* L1cache){
  CacheBlock* curBlock;
  CacheBlock* lastBlock;
  CacheBlock* prevLastBlock; 
  curBlock = &L1cache->cacheBlocks[cacheInx][L1cache->numWays-1]; 
  //Check if last block in set is valid
  if(curBlock->isValid){
    //This means all blocks in set are valid as well
    return NULL; 
  }
  //There's at least one invalid block in set
  else{
    //Check if first block is invalid to return it right away, also means all blocks are invalid in set
    curBlock = &L1cache->cacheBlocks[cacheInx][0];
    if(!curBlock->isValid){
      return curBlock; 
    }
    //Shift the values of the blocks in the set by one towards the end to return the first block
    //in set, and keep LRU order.
    else{ 
      //Waterfall the values of the valid blocks from start of the set (low indexes) to the end of the set (higher indexes)
      for(int i=L1cache->numWays-1;i>0;i--){
	lastBlock = &L1cache->cacheBlocks[cacheInx][i];
	prevLastBlock = &L1cache->cacheBlocks[cacheInx][i-1];
	copyBlocks(prevLastBlock,lastBlock); 
      }
      //Return first block in set to be possibly writen back and updated
      curBlock = &L1cache->cacheBlocks[cacheInx][0]; 
      return curBlock; 
    }
  } 
}

//update the cache block with data and all that good stuff. 
void updateBlock(long long unsigned opTag, long long unsigned opCacheInx, VirtIns* op, CacheBlock* block, Cache* cache){
  //printf("We're updating block with tag: %#llx from op: \n",opTag); 
  //printOp(op); 
  //Maintain the FIFO for evictions by checking if block is at front of set (index 0)
  CacheBlock* frontBlock = &cache->cacheBlocks[opCacheInx][0]; 
  CacheBlock tempBlock; 
  CacheBlock* lastBlock;
  CacheBlock* prevLastBlock;
  
  //If the block we're updating 'block' is not the front block we know it's because 'block' comes from an eviction, and we can shift all the values down the set to place block in front
  if(frontBlock->tag!=block->tag){
    copyBlocks(block,&tempBlock); 
    //Waterfall the values of the valid blocks from start of the set (low indexes) to the end of the set (higher indexes)
    for(int i=cache->numWays-1;i>0;i--){
      lastBlock = &cache->cacheBlocks[opCacheInx][i];
      prevLastBlock = &cache->cacheBlocks[opCacheInx][i-1];
      copyBlocks(prevLastBlock,lastBlock); 
    }			
    //Save values back to the front block
    copyBlocks(&tempBlock,frontBlock); 
    //Make 'block' point to the front block 
    block = frontBlock; 
  }
  
  //Check if read or write
  if(op->isWrite){
    block->isValid= true; 
    block->isDirty= true; 
    block->addr   = op->addr; 
    block->tag    = opTag; 
    memcpy((void*)block->data, (void*)op->data, 64); 
  }
  else{
    block->isValid= true; 
    block->addr   = op->addr;
    block->tag    = opTag; 
  }
}

//Find a block in the cache to evict, and return a pointer to it  
CacheBlock* getBlockToEvict(long long unsigned opCacheInx, Cache* L1cache){
  //Return the last block in the set FIFO. When this function is called we know all blocks are
  //valid in the set, and the last one is the LRU block because of how blocks are shuffled when a set is accessed with checkForInvalidWays() and checkForBlock()
  return &L1cache->cacheBlocks[opCacheInx][L1cache->numWays-1]; 
}  


void updateDRAMhist(CacheBlock* block){
  writeBacks++; 
  //make the data update from cache block's attributes
  DRAMupdate* dUp = new DRAMupdate;
  //DRAMupdate testDup; 
  memcpy((void*)dUp->data,(void*)block->data,64);
  
  //Check if there's a value at the key address
  auto t = DRAMhist.find(block->addr); 
  if(t == DRAMhist.end()){  //testDup==NULL){
    dUp->nextUpdate = dUp; 
    dUp->lastUpdate = dUp; 
    DRAMhist[block->addr]=dUp; 
  }
  else{
    (t->second->lastUpdate)->nextUpdate = dUp; 
    t->second->lastUpdate=dUp; 
  }
}

//Write back the input block (This would be exteremely easy if the cache ran within pintool)
void cbWriteBack(CacheBlock* block){
  updateDRAMhist(block); 
  //Reset blocks attributes
  block->isValid = false;
  block->isDirty = false; 
  //block->data    = NULL; 
}

//Print data to an input file and end it with a new line character
void fprintData(FILE* file, long long* data){
  for(int i=0; i<8; i++){
    fprintf(file, "%#llx\n",data[i]); 
  }
  fprintf(file, "\n"); 
}


void writeDRAMhist(char* traceFileName){
  FILE* DRAMtrace; 
  DRAMupdate* curUp; 
  DRAMtrace = fopen(traceFileName, "w");
  //Iterate through keys
  for(auto const &entry : DRAMhist){
    fprintf(DRAMtrace, "Address: %#llx\n", entry.first); 
    fprintf(DRAMtrace, "Data: \n"); 
    curUp = entry.second; 
    //Edge case 1: linked list is 1 in size
    if(curUp==(entry.second)->lastUpdate){
      fprintData(DRAMtrace,curUp->data); 
    }
    //Edge case 2: Print first, middle and last elements in linked list
    else{
      //entry.second points to the beginning of a linked list of data updates
      //for each key, print out linked list of data updates to same key/address
      while(curUp != (entry.second)->lastUpdate){
	fprintData(DRAMtrace, curUp->data); 
	curUp = curUp->nextUpdate; 
      }
      //Print last last node in list that broke the while loop
      fprintData(DRAMtrace,curUp->data); 
    }
  }
  fclose(DRAMtrace); 
}


//remove memory allocated for the DRAMupdates
void cleanUpDRAMupdates(){
  DRAMupdate* cur; 
  DRAMupdate* next; 
  //Iterate through keys
  for(auto const &entry : DRAMhist){
    cur = entry.second;
    //Edge case 1: linked list is 1 in size
    if(cur==(entry.second)->lastUpdate){
      delete [] cur->data; 
      delete cur;  
    }
    //Edge case 2: Print first, middle and last elements in linked list
    else{
      //entry.second points to the beginning of a linked list of data updates
      //for each key, print out linked list of data updates to same key/address
      cur=entry.second->nextUpdate; 
      while(cur != (entry.second)->lastUpdate){
	//fprintData(DRAMtrace, curUp->data); 
	next = cur->nextUpdate; 
	delete [] cur->data; 
	delete cur;
	cur = next; 
      }
      //Print last last node in list that broke the while loop
      //fprintData(DRAMtrace,curUp->data); 
      delete [] entry.second->data; 
      delete entry.second; 
      delete [] cur->data;
      delete cur; 
    }
  }
} 

//Remove memory allocated for the cache.
void cleanUpCache(Cache* cache){
  int sets = (cache->size/64)/cache->numWays;
  for(int i=0; i< sets; i++){
    for(int j=0; j<cache->numWays; j++){
      delete [] (cache->cacheBlocks[i][j]).data; 
      //delete cache->cacheBlocks[i][j]; 
    }
    delete [] cache->cacheBlocks[i]; 
  }
  delete cache; 
} 

//-----------------------------------------------------------------------------
//--------------------Main function: Calls helper functions--------------------
//-----------------------------------------------------------------------------
int main(int argc, char* argv[]){
  //Local vars 
  bool isPresent;    //is block present in the cache?
  bool eofReached;   //to check if we got to the end of the virtual trace file
  long long unsigned int opTag; 
  long long unsigned int opCacheInx;
  struct Cache* L1cache; //Can make this an array of caches later
  int size= atoi(argv[1]); 
  int ways= atoi(argv[2]);
  virtTrace.open(argv[3]);
  char* DRAMtraceOutFile = argv[4]; 
  long long int* blockData;
  struct CacheBlock* block;
  struct VirtIns* op = new VirtIns;
 

  //initiallize the block structs in the cache
  L1cache = initCache(size, ways); 
  
  //New operation/instruction from virtual trace
  while(getOp(op, &virtTrace)){ 
    //Get cache index and tag (to put the new memory operation into the cache)
    opTag = getTag(op, size, ways); 
    opCacheInx = getInx(op, size, ways);
    //printf("tag: %llu\n",opTag); 
    //printf("index: %llu\n",opCacheInx); 
    
    //Check if block is already in cache
    
    //printOp(op); //DB
    
    block = checkForBlock(opTag, opCacheInx, L1cache); 
    //block is present (block points to present block or NULL if not present.)
    if(block){
      updateBlock(opTag,  opCacheInx, op, block,L1cache); 
    }
    //block not present (cache miss)
    else{
      //Check the miss that just happened
      if(op->isWrite) writeMisses++; 
      else readMisses++; 
      //Print miss
      //printf("Just missed on tag: %llx\n",opTag); 

      //Check for invalid ways (in the indexed cache set)
      block = checkForInvalidWays(opTag, opCacheInx, L1cache); 
      //Invalid ways (block points to an invalid block if there's an invalid way, otherwise block points to NULL)
      if(block){
	updateBlock(opTag,  opCacheInx, op, block,L1cache); 
      }
      //no open ways (block = null)
      else{
	//Evict a block (to save new op into it)
	block = getBlockToEvict(opCacheInx, L1cache);//Returns pointer to an evicted block	
	//Check if block is dirty (so we can write it back if it is)
	if(block->isDirty){
	  cbWriteBack(block);
	}
	//Update the block with new update (regardless of block state)
	updateBlock(opTag, opCacheInx, op, block,L1cache);
      }
    } 
  }
  
  writeDRAMhist(DRAMtraceOutFile);  
  //----------Use with CacheTest4-----------
  //Check that every block in cache is invalid
  int invalidCnt=0; 
  for(int i=0;i<(L1cache->size/L1cache->numWays)/64;i++){
    for(int j=0;j<L1cache->numWays;j++){
      if(!L1cache->cacheBlocks[i][j].isValid){
	//printf("We found an invalid block: This is wrong!!!\n"); 
	invalidCnt++; 
      }
    }
  }
  printf("Invalid blocks found: %d\n",invalidCnt); 
  //-----------------------------------------
  printf("Memory Ops: %d\n", memOps); 
  printf("writebacks: %d\n", writeBacks);
  printf("Read Misses: %d\n", readMisses); 
  printf("Write Misses: %d\n", writeMisses);
  
  
  //Clean up memory
  //cleanUpDRAMupdates();
  //printf("Cleaned up DRAMupdates\n"); 
  //cleanUpCache(L1cache); 
  //printf("cleaned up cache"); 
  delete op; 
  return 0; 
}
