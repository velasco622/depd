/*
Edited by Alfredo J. Velasco 7/19/2015 
For project: DRAM-ECC PCM-Data
Description: Make file with a wave of memory requetss where every block is writen to check if 
every block in cache is invalid after writing to it. 
Inputs: size and ways of the cache to produce the test for
Outputs: Dummy file at this path: 
"/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest4.txt" 
with the name number of input writes with the following format:
 
R
[addr]
[aligned Addr]
\n
W
[addr]
[aligned addr]
\n
.
.
.
R
[addr]
[aligned addr] (no new line character after the last line of addr or data)
 */

#include "CacheTests.h"

int main(int argc, char* argv[]){
  //Set up
  FILE* outFile; 
  outFile = fopen("/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest4.txt", "w");
  int size = atoi(argv[1]); 
  int ways = atoi(argv[2]); 
 
  //wave of memory requests 1: print writes of 0's to every block to fill the cache
  pFillWrites(outFile,size,ways); 

}
