#!/bin/bash

#Edited by Alfredo J. Velasco 8/3/15
#Project: DEPD
#Script Description: Commands to produce the metrics/figures of merit for DEPD project


echo '-----------------------Benchmark File: sorted_bs_native.txt ---------------------------------'
echo 'Number of '
echo '   Addresses with updates >= 2 (silent writes+valid updates):' 
ups2orMore=`awk '{ print $1 }' /usr/xtmp/alfredo/pintoolResults/sorted_bs_native.txt | paste -sd+ | bc`
echo '   '$ups2orMore

echo '   Addresses with valid updates (more than one non-silent update)'
validUps=`wc -l /usr/xtmp/alfredo/pintoolResults/sorted_bs_native.txt | cut -f1 -d' '`
echo '   '$validUps

echo '   Silent Writes:'
silentWrites=$[ups2orMore-validUps]
echo '   '$silentWrites

echo List of benchmark files: 
for i in $( ls /usr/xtmp/alfredo/pintoolResults/sorted_*_native.txt ); do 
    echo $i



















done
