/*
Define the function to calculate the check bits for a secded (72,64) code. 

*/

#include "stdlib.h"
#include "stdio.h"
#define LEN(x)  (sizeof(x) / sizeof((x)[0]))

int main(int argc, char *argv[]){
  //Save the arrays with the right indexes within the data to xor for each check bit P0-P6
  int P0inxs[35]={
    0,1,3,4,6,8,10,11,13,15,17,19,21,23,25,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,57,59,61,63
  };
  int P1inxs[35]={
    0,2,3,5,6,9,10,12,13,16,17,20,21,24,25,27,28,31,32,35,36,39,40,43,44,47,48,51,52,55,56,58,59,62,63
  };
  int P2inxs[35]={
    1,2,3,7,8,9,10,14,15,16,17,22,23,24,25,29,30,31,32,37,38,39,40,45,46,47,48,53,54,55,56,60,61,62,63
  };
  int P3inxs[31]={
    4,5,6,7,8,9,10,18,19,20,21,22,23,24,25,33,34,35,36,37,38,39,40,49,50,51,52,53,54,55,56
  };
  int P4inxs[33]={
    11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
  };
  int P5inxs[33]={
    26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
  };
  int P6inxs[7]={
    57,58,59,60,61,62,63
  };
  int P[8];
  int *Pinxs[7]={
    P0inxs, P1inxs, P2inxs, P3inxs, P4inxs, P5inxs, P6inxs
  }; 
  int PinxsSize[7]={
    35,35,35,31,33,33,7
  };

  //---------------------Get P0-P6-------------------
  int i;
  int j; 
  unsigned long int data = strtoul(argv[1],NULL,16); 
  for(j = 0; j< LEN(P)-1; j++){ 
    P[j] = (data>>Pinxs[j][0])%2; 
    for(i = 1; i<PinxsSize[j]; i++){
      P[j] = P[j] ^ ((data>>Pinxs[j][i])%2);
    }
  }
  //---------------------Get P7----------------------
  //XOR P7 with all the data bits
  P[7] = data%2;
  for(i=1; i<64; i++){
    P[7]=P[7] ^ ((data>>i)%2);
  }
  //XOR P7 with all the check bits
  for(i=0; i<7; i++){
    P[7]=P[7] ^ P[i];
  }
  /*Use below to check for correctness. 
  printf("These are the checkbits from P7-P0: ");
  for(i = 7; i>=0; i--){
    printf("%d", P[i]);
  }
  printf("\nCheckbits should be from P7-P0:     %s\n",argv[2]);
  */
}
