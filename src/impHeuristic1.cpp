/*
Edited by Alfredo J. Velasco 8/10/2015 
For project: DRAM-ECC PCM-Data
Description: Source code for the implementation of the first DEPD heuristic of Experiment 2. 
In this heuristic, we implement partial and comprehensive updates. Partial updates only update
the bits that are not what they should already be given an update, but this gives rise to 
disturbed bits by thermal crosstalk. When enough disturbed bits accumulate, a comprehensive 
udpate is required to insure correctness incurring additional programms to cells. A partial update 
is always made first, then the disturbed cells are tallied to see if a comprehensive update is necessary. 
If comprehensive update is not needed, we continue updating partially until it is. 

We perfrom these tests for different levels of 
protection, that is, with different numbers of protected bits with ECC, 1, 2, ... n. Where n is
an input by the user. 
Inputs: [PATHtoDRAMtraceFile] [NumberOfBitsToProtect]
Outpus: Message specifying different figures of merit related to the heuristic

-----------------------------------Sample output-----------------------------------------------
----* 'N' is the number of bits protected by ECC *----
N = 1
-Total number of programmed cells:              [SomeNumber]=totPartUpBits+totCompUps*64
-Total number of disturbed Cells :             [SomeNumber]
-Total number of comprehensive updates:         [SomeNumber]
-Average number of disturbed cells per update: [SomeNumber]
-Average ratio of partially udpated bits per update:
-Successful partial updates   (partial updates that are not followed by a comprehensive update)   :                                                   [SomeNumber]
-Unsuccessful partial updates (partial updates that are directly followed by a comprehensive update, these are a waste of programs)     :             [SomeNumber]
-Average number of successful partial updates before 
comprehensive update            :             [SomeNumber]

*/

#include "processTraces.h"

//--------------------Global Vars---------------------------------------
std::ifstream DRAMtrace; 
unsigned long long addr1; 
unsigned long long addr2; 
long int wordCnt=0; 
long int eccWordCnt=0;
bool isFirstPair;
long int eccCnt=0;

int main(int argc, char* argv[]){
 
  DRAMtrace.open(argv[1]);
  int maxEccProtection = atoi(argv[2]);
  //Initialize structures to keep our data and ECC
  unsigned long long *dataPair[2];//= new unsigned long long*[2]; 
  float totDataAvg=0; 
  float totECCAvg=0; 
  unsigned long long data1[8];//= new unsigned long long[8];  
  unsigned long long data2[8];//= new unsigned long long[8];
  dataPair[0]=data1;
  dataPair[1]=data2;
  //Counting and avaraging variables. 
  unsigned long pairCnt=0;
  double totPartUpBitsRatio=0; //Count the number of partially updated bits
  unsigned long disturbCnt=0;
  unsigned long long prevAddr=0;
  unsigned long long curAddr=0;
  int curDistrubCnt=0;
  int curPartUpBits=0;
  int curPartUpCnt=0;
  float curPartUpBitsRatio=0;
  unsigned long totPartUpBits=0;
  int newDisturbCnt=0;
  int disturbAtAddress=0;
  unsigned long totDisturbCnt=0;
  unsigned long totCompUpCnt=0;
  unsigned long unsPartUpsCnt=0;
  unsigned long successPartUpsCnt=0;
  double avgPartUpBitsRatio=0;
  double avgPartUpBits=0;
  double avgDisturbCnt=0;
  double avgSuccessPartUpsBeforeCompUp=0;
  double partUpsToCompUpsRatio=0;
  const int bitBlockSize=512;
  int dstScoreBoard[bitBlockSize]={0};

  //Get a pair of data to compare
  while(getDataPair(dataPair,pairCnt)){
    //Check the address of the update and reset dstScoreBoard can be reset when a new address is updated
    prevAddr = curAddr;
    curAddr = addr1; 
    if(curAddr!=prevAddr){
      resetScoreB(dstScoreBoard,bitBlockSize);
    }
    
    //Set current, (per update) counters to 0
    newDisturbCnt=0;
    curPartUpBits=0;
    curPartUpBitsRatio=0;
    curPartUpCnt=0;

    //Make the partial update (pretend it happened) and count the number of bits updated
    curPartUpBits=cntPartiallyUpdatedBits(dataPair); 
    curPartUpBitsRatio=(double)curPartUpBits/bitBlockSize; 
    totPartUpBits+=curPartUpBits;
    totPartUpBitsRatio+=curPartUpBitsRatio; 

    pairCnt++; 
    
    //Tally disturbed cells
    newDisturbCnt=tallyUpdateDisturb(dataPair,dstScoreBoard);
    totDisturbCnt+=newDisturbCnt;
    
    disturbAtAddress=tallyAddressDisturb(dataPair[1],dstScoreBoard,bitBlockSize);
    
    //Make comprehensive update if necessary
    if(disturbAtAddress > maxEccProtection){      
      //reset dstScoreBoard[]
      resetScoreB(dstScoreBoard,bitBlockSize);
      totCompUpCnt++; 
      if(curPartUpCnt==0){
	unsPartUpsCnt++;
      }
    }
    else{
      curPartUpCnt++; 
      successPartUpsCnt++;
    }
    
    //Get a new pair
  }
  avgPartUpBitsRatio=totPartUpBitsRatio/pairCnt;
  avgPartUpBits=(double)totPartUpBits/pairCnt;
  avgDisturbCnt=(double)totDisturbCnt/pairCnt;
  partUpsToCompUpsRatio=(double)successPartUpsCnt/totCompUpCnt;
  
  printf("Max number of ECC protected bits: %d\n",maxEccProtection);
  printf("Total number of programmed cells: %lu\n",pairCnt*bitBlockSize-totPartUpBits+totCompUpCnt*bitBlockSize);
  printf("Total number of disturbed Cells : %lu\n",totDisturbCnt);
  printf("Total number of comprehensive updates: %lu\n", totCompUpCnt);
  printf("Average disturbed cells per update: %f\n",avgDisturbCnt);
  printf("Avarage ratio of partially updated bits per update: %f\n",avgPartUpBitsRatio);
  printf("Successful partial updates: %lu\n",successPartUpsCnt);
  printf("Unsuccessfull partial updates: %lu\n",unsPartUpsCnt);
  printf("Ratio of partial updates to comprehensive: %f\n",partUpsToCompUpsRatio);
  printf("Number of natural Fixes: %ld\n",naturalFixCnt);
  
  return 0; 
}
