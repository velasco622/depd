/*
Edited by Alfredo J. Velasco 7/19/2015 
For project: DRAM-ECC PCM-Data
Description: These functions are used in the main of the different Cache tests 
*/

#include "CacheTests.h"

//----------------------Both functions below make accesses in order that is, go to an address and increment the tags to fill up the entire line before going to the next line, starting at line 0.
//-----------------------------------------------------------------------------
//print writes of 0's to every block in the cache in order, from lower to 
//uper addresses
void pFillWrites(FILE* outFile,int size,int ways){
  int blockSize = 64; 
  int numBlocks = (size/blockSize)/ways; 
  long long unsigned int addr; 
  
  //Go through the different cache lines
  for(int a=0;a<numBlocks;a++){
    //Set up the address
    addr = a*blockSize; 
    
    //Make a write to every way in the address
    for(int w=0;w<ways;w++){
      
      //Print the write access
      fprintf(outFile,"W\n");
      fprintf(outFile,"%#llx\n",addr);
      fprintf(outFile,"%#llx\n",addr); 
      //Print 0's for data
      for(int i=0;i<8;i++){
	fprintf(outFile,"0\n"); 
      }
      fprintf(outFile,"\n"); 

      //Update the tag to get a different way
      addr+=( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ); 
    }
  }
} 

//Make read operations in tag order to show no eviction happens of the writen blocks
void pOrdReads(FILE* outFile, int size, int ways){
  int blockSize = 64; 
  long long unsigned int addr;
  int numBlocks = (size/blockSize)/ways;  

  //Go through the different cache lines
  for(int a=0;a<numBlocks;a++){
    //Set up the address
    addr = a*blockSize; 
    
    //Make a write to every way in the address
    for(int w=0;w<ways;w++){
      
      //Print the write access
      fprintf(outFile,"R\n");
      fprintf(outFile,"%#llx\n",addr);
      fprintf(outFile,"%#llx\n",addr); 
      //Print 0's for data
      //for(int i=0;i<8;i++){
      //fprintf(outFile,"0\n"); 
      //}
      
      //Only print out if you're not in the last block
      if( !(w==(ways-1) &&  (a*blockSize == size-blockSize)) ){
	fprintf(outFile,"\n"); 
      }
      //Update the tag to get a different way
      addr+=( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ); 
    }
  }
}

//write the cache with an offset tag. Instead of reading/writing with tag of 0->'ways'-1, this function makes read requests with tag 'ways'->(2*'ways'-1)
void pOffSetWrite(FILE* outFile, int size, int ways){
  int blockSize = 64; 
  long long unsigned int addr;
  int numBlocks = (size/blockSize)/ways;  

  //Go through the different cache lines
  for(int a=0;a<numBlocks;a++){
    //Set up the address
    addr = a*blockSize; 
    
    //Set up address so that the tag will start at 'ways'
    addr+= ( ( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ) * ways) ; 
    //Make a write to every way in the address at an offset
    for(int w=0;w<ways;w++){
      //Print the write access
      fprintf(outFile,"W\n");
      fprintf(outFile,"%#llx\n",addr);
      fprintf(outFile,"%#llx\n",addr); 
      //Print 0's for data
      for(int i=0;i<8;i++){
	fprintf(outFile,"0\n"); 
      }
      //Update the tag to get a different way
      addr+=( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ); 
      //Only print out if you're not in the last block
      if( !(w==(ways-1) &&  (a*blockSize == size-blockSize)) ){
	fprintf(outFile,"\n"); 
      } 
    }
  }
}

//Write to same set over and over
void pWritesToSet(FILE* outFile, int size, int ways, int set, int reps){
  int blockSize=64;
  long long unsigned addr = set*blockSize; 
  int numBlocks = (size/blockSize)/ways; 
  //Check that the input set makes sense
  if(set>numBlocks){
    printf("The set entered is too large! make it less than %d\n", numBlocks);
    exit(EXIT_FAILURE); 
  }
  
  //Hammer on the address for the amount of repetitions
  for(int i=0;i<reps;i++){
    //Make a write to every way in the set
    for(int w=0;w<ways;w++){
      //Print the write access
      fprintf(outFile,"W\n");
      fprintf(outFile,"%#llx\n",addr);
      fprintf(outFile,"%#llx\n",addr); 
      //Print 0's for data
      for(int i=0;i<8;i++){
	fprintf(outFile,"0\n"); 
      }  
      //Only print out if you're not in the last block
      //if( !(w==(ways-1)) ){
      fprintf(outFile,"\n"); 
      //}
      //Update the tag to get a different way
      addr+=( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ); 
    }
    //Reset address
    addr = set*blockSize; 
  }
}

//Write to same set over and over, but with one extra write than the number of ways
void pWritesP1ToSet(FILE*outFile, int size, int ways, int set, int reps){
  int blockSize=64;
  long long unsigned addr = set*blockSize; 
  int numBlocks = (size/blockSize)/ways; 
  //Check that the input set makes sense
  if(set>numBlocks){
    printf("The set entered is too large! make it less than %d\n", numBlocks);
    exit(EXIT_FAILURE); 
  }
  
  //Hammer on the address for the amount of repetitions
  for(int i=0;i<reps;i++){
    //Make a write to every way in the set with an additional write to
    for(int w=0;w<ways+1;w++){
      //Print the write access
      fprintf(outFile,"W\n");
      fprintf(outFile,"%#llx\n",addr);
      fprintf(outFile,"%#llx\n",addr); 
      //Print 0's for data
      for(int i=0;i<8;i++){
	fprintf(outFile,"0\n"); 
      }  
      //Only print out if you're not in the last block
      //if( !(w==(ways-1)) ){
      fprintf(outFile,"\n"); 
	//     }
      //Update the tag to get a different way
      addr+=( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ); 
    }
    //Reset address
    addr = set*blockSize; 
  }
}

//print writes to half of the cache with ways going from ways to 2*ways-1
void pFillHalfWritesDifTag(FILE*outFile,int size,int ways){
  int blockSize = 64; 
  long long unsigned int addr;
  int numBlocks = (size/blockSize)/ways;  

  //Go through half of the blocks in cache
  for(int a=0;a<numBlocks/2;a++){
    //Set up the address
    addr = a*blockSize; 
    
    //Set up address so that the tag will start at 'ways'
    addr+= ( ( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ) * ways) ; 
    //Make a write to every way in the address at an offset
    for(int w=0;w<ways;w++){
      //Print the write access
      fprintf(outFile,"W\n");
      fprintf(outFile,"%#llx\n",addr);
      fprintf(outFile,"%#llx\n",addr); 
      //Print 0's for data
      for(int i=0;i<8;i++){
	fprintf(outFile,"0\n"); 
      }
      //Update the tag to get a different way
      addr+=( 1<<((int)log2(blockSize)+(int)log2(numBlocks)) ); 
      //Only print out if you're not in the last block
      if( !(w==(ways-1) &&  (a*blockSize == size-blockSize)) ){
	fprintf(outFile,"\n"); 
      } 
    }
  }
}
