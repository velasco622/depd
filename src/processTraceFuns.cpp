/*
Edited by Alfredo J. Velasco 8/10/2015 
For project: DRAM-ECC PCM-Data (DEPD)
Description: These are all the function definitions for processing the traces for Experiment 1 
and 2 for the DEPD paper. 
 */

#include "processTraces.h"

long int naturalFixCnt=0;

//-----------------------------------------------------------------------
//---------------------Heuristics Helper functions-----------------------
//-----------------------------------------------------------------------
//Use to set every element in the input array to 0's
void resetScoreB(int* dstScoreBoard, int lenOfScoreB){ 
  for(int i=0; i<lenOfScoreB;i++){
    dstScoreBoard[i]=0;
  }
}

//Counts the number of bits that are partially updated, 
//That is, the number of bits that are the same across the update
int cntPartiallyUpdatedBits(unsigned long long** pair){
  unsigned long long c;
  //int j=0;
  //float avg=0;
  int cnt=0;
  int numIters=sizeof(unsigned long long)*8;
  int numWords=8;
  //char cStr[sizeof(unsigned long long)*8+1];                    
  for(int i=0;i<numWords;i++){
    c = pair[0][i]^pair[1][i];
    //Count number of 0's in c; i.e. number of bits that stayed the same across updates                           
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) == 0){
        cnt++;
      }
    }
  }

  return cnt;
}

//Find the number of 1's in the input array of data. These are the cells that do not have to be programmed because a selective update only updates 0's.      
int tallySelecCompProgSaved(unsigned long long int* newData){
  int cnt=0;
  int numIters=sizeof(unsigned long long)*8;
  int numWords=8;
  
  for(int i=0;i<numWords;i++){
    //Count number of 1's in newPair; i.e. number of bits that will not be updated because of selective update
    for(int j=0; j<numIters; j++){
      if( (newData[i] & (1ULL << j)) != 0){
        cnt++;
      }
    }
  }
  return cnt;
}

//Find the number of set cells that stay set across the update, (1's that are 1's in both pairs of data). 
int tallyOnes(unsigned long long** pair){
  unsigned long long c;
  int cnt=0;
  int numIters=sizeof(unsigned long long)*8;
  int numWords=8;
  //char cStr[sizeof(unsigned long long)*8+1];                    
  for(int i=0;i<numWords;i++){
    c = pair[0][i]&pair[1][i];
    //Count number of 1's in c; i.e. number of bits that stayed set, i.e. 1 across updates                
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) != 0){
        cnt++;
      }
    }
  }

  return cnt;
}

//Find the number of disturbed bits in the update
int tallyUpdateDisturb(unsigned long long** dataPair,int* dstScoreBoard){
  int numWords = 8;//Each word is 8 bytes, and there's 8 words per cache block.
  int numBits = 64;
  int disturbCnt=0;
  //Start from the rightmost word in the data pair, as well as the rightmost bit
  for(int wordIndex=numWords-1; wordIndex>=0;wordIndex--){
    for(int bitIndex=0; bitIndex<numBits;bitIndex++){
      //Look for a 0/reset cell that is a zero in both dataPair[0] and dataPair[1]
      if( ( (dataPair[0][wordIndex] & (1ULL << bitIndex)) == 0 ) && ( (dataPair[1][wordIndex] & (1ULL << bitIndex)) == 0 ) ){
	//Edge case 1: This zero is the rightmost bit in the word
	if(bitIndex==0){
	  if(wordIndex == numWords-1){
	    //---Edge case 1.1: This word is the rightmost word in the data
	    //No disturb from the right possible
	
	    //---Look to the left for disturb---
	    //Check if bit to the left is a 1
	    if( (dataPair[0][wordIndex]) & (1ULL<< (bitIndex+1) ) != 0){
	      //check if this 1 bit is reset in dataPair[1]
	      if( (dataPair[1][wordIndex]) & (1ULL<< (bitIndex+1) ) == 0 ){
		//The original 0 in dataPair[0] is disturbed            
                //Check if this original bit in dataPair[0] is not yet disturbed       
		if( dstScoreBoard[wordIndex*numBits + (numBits-1)]==0){
		  disturbCnt++;
		  dstScoreBoard[wordIndex*numBits + (numBits-1)]=1;
		}
	      }
	    }
	  }
	  //---Edge case 1.2: This word is not rightmost word in data
	  else{
	    //---Look to the right for disturb--
	    //Check if leftmost bit in the word to the right is a 1
	    if( (dataPair[0][wordIndex+1]&(1ULL<<(numBits-1))) != 0){
	      //Check if this 1 bit is reset in dataPair[1]
	      if( (dataPair[1][wordIndex+1]&(1ULL<<(numBits-1))) == 0){
	  	//The original 0 in dataPair[0] is disturbed
		//Check if this original bit in dataPair[0] is not yet disturbed
		if(dstScoreBoard[wordIndex*numBits + (numBits-1)]==0){
		  disturbCnt++;
		  dstScoreBoard[wordIndex*numBits + (numBits-1)]=1;
		}
	      }
	    }
	    //---Look to the left for disturb---
	    //Check if bit to the left is a 1
	    if( (dataPair[0][wordIndex]) & (1ULL<< (bitIndex+1) ) != 0){
	      //check if this 1 bit is reset in dataPair[1]
	      if( (dataPair[1][wordIndex]) & (1ULL<< (bitIndex+1) ) == 0 ){
		//The original 0 in dataPair[0] is disturbed            
                //Check if this original bit in dataPair[0] is not yet disturbed       
		if(dstScoreBoard[wordIndex*numBits + (numBits-1)]==0){
		  disturbCnt++;
		  dstScoreBoard[wordIndex*numBits + (numBits-1)]=1;
		}
	      }
	    }
	  }
	}

	//Edge case 2: This zero is the leftmost bit in the word
	else if(bitIndex==numBits-1){
	  //Edge case 2.1: Word is leftmost word in data
	  if(wordIndex==0){
	    //No disturbance from left possible
	    
	    //---Look to the right for disturb---
	    //Check if bit to the right is a 1
	    if( (dataPair[0][wordIndex] & (1ULL<<(numBits-2))) !=0){
	      //Check if this 1 bit is reset in dataPair[1]
	      if( (dataPair[1][wordIndex] & (1ULL<<(numBits-2))) == 0 ){
		//Check if this original bit in dataPair[0] is not yet disturbed       
		if(dstScoreBoard[wordIndex*numBits]==0){
		  disturbCnt++;
		  dstScoreBoard[wordIndex*numBits]=1;
		}
	      }
	    }
	  }
	  //Edge case 2.2: Word is not leftmost word in data
	  else{
	    //---Look to the right for disturb---
	    //Check if bit to the right is a 1
	    if( (dataPair[0][wordIndex] & (1ULL<<(numBits-2))) !=0){
	      //Check if this 1 bit is reset in dataPair[1]
	      if( (dataPair[1][wordIndex] & (1ULL<<(numBits-2))) == 0 ){
		//Check if this original bit in dataPair[0] is not yet disturbed       
		if(dstScoreBoard[wordIndex*numBits]==0){
		  disturbCnt++;
		  dstScoreBoard[wordIndex*numBits]=1;
		}
	      }
	    }
	    //---Look to the left for disturb---
	    if( (dataPair[0][wordIndex-1]& (1ULL)) != 0){
	      //Check if this 1 bit is reset in dataPair[1]
	      if( (dataPair[1][wordIndex-1]& (1ULL)) == 0){
		//Check if this original bit in dataPair[0] is not yet disturbed       
		if(dstScoreBoard[wordIndex*numBits]==0){
		  disturbCnt++;
		  dstScoreBoard[wordIndex*numBits]=1; 
		}
	      }
	    }
	  }
	}
	
	//Edge case 3: This zero is somewhere in the middle of the word
	else{
	  //---Look to the right for disturb---
	  //Check if bit to the right is a 1
	  if( (dataPair[0][wordIndex] & (1ULL<<(bitIndex-1))) != 0 ){
	    //Check if this 1 bit is reset in dataPair[1]
	    if( (dataPair[1][wordIndex] & (1ULL<<(bitIndex-1))) == 0){
	      //Check if this original bit in dataPair[0] is not yet disturbed
	      if(dstScoreBoard[wordIndex*numBits + ( (numBits-1) - bitIndex )]==0){
		disturbCnt++;
		dstScoreBoard[wordIndex*numBits + ( (numBits-1) - bitIndex )]=1;
	      }
	    }
	  }
	  //---Look to the left for disturb---
	  //Check if bit to the left is a 1
	  if( (dataPair[0][wordIndex] & (1ULL<<(bitIndex+1))) !=0 ){
	    //check if this 1 bit is reset to 0 in dataPair[1]
	    if( (dataPair[1][wordIndex] & (1ULL<<(bitIndex+1))) == 0){
	      //Check if original bit in dataPair[0] has already been disturbed
	      if( dstScoreBoard[wordIndex*numBits + ( (numBits-1) - bitIndex )] == 0){
		disturbCnt++;
		dstScoreBoard[wordIndex*numBits + ( (numBits-1) - bitIndex )]=1;
	      }
	    }
	  }
	}
      }
    }
  }
  return disturbCnt;
}

//Find the number of possible indirect updates. These are cells being updated from 0->1 that also have at least one 0 next to them after the update. For example, if we had two cells, cell a and cell b, where a goes from 0->1 and b goes from 0->0 cell a is a possible indirect update, because by going from 0->0, b would set cell a through thermal crosstalk. 
int tallyPossIndirectUps(unsigned long long** dataPair){
  int numWords = 8;//Each word is 8 bytes, and there's 8 words per cache block.
  int numBits = 64;
  int indirectUpsCnt=0;
  //Start from the rightmost word in the data pair, as well as the rightmost bit
  for(int wordIndex=numWords-1; wordIndex>=0;wordIndex--){
    for(int bitIndex=0; bitIndex<numBits;bitIndex++){
      //Look for a 0/reset cell that is a zero in dataPair[0] and one in dataPair[1]
      if( ( (dataPair[0][wordIndex] & (1ULL << bitIndex)) == 0 ) && ( (dataPair[1][wordIndex] & (1ULL << bitIndex)) != 0 ) ){
	//Edge case 1: This zero is the rightmost bit in the word
	if(bitIndex==0){
	  if(wordIndex == numWords-1){
	    //---Edge case 1.1: This word is the rightmost word in the data
	    //No zeros from the right possible
	
	    //---Look to the left for a 0---
	    //Check if bit to the left is a 0 in the update
	    if( (dataPair[1][wordIndex]) & (1ULL<< (bitIndex+1) ) == 0 ){           		
	      indirectUpsCnt++;
	      //disturbCnt++;
	    }
	  }

	  //---Edge case 1.2: This is word is not rightmost word in data
	  else{
	    //---Look to the right and to the left for a 0--
	    //Check for adjecent 0 in dataPair[1]
	    if( ((dataPair[1][wordIndex+1]&(1ULL<<(numBits-1))) == 0 )||( (dataPair[1][wordIndex]) & (1ULL<< (bitIndex+1) ) == 0) ){	      
	      indirectUpsCnt++;	
	      //disturbCnt++;
	    }
	  }
	}

	//Edge case 2: This zero is the leftmost bit in the word
	else if(bitIndex==numBits-1){
	  //Edge case 2.1: Word is leftmost word in data
	  if(wordIndex==0){
	    //No disturbance from the left possible
	    
	    //---Look to the right for a 0---
	    if( (dataPair[1][wordIndex] & (1ULL<<(numBits-2))) == 0 ){   
	      indirectUpsCnt++;
	      //disturbCnt++;
	    }
	  }
	  //Edge case 2.2: Word is not leftmost word in data
	  else{
	    //---Look to the right and left for a 0---
	    if( ( (dataPair[1][wordIndex] & (1ULL<<(numBits-2))) == 0 )||( (dataPair[1][wordIndex-1]& (1ULL)) == 0 ) ){
	      indirectUpsCnt++;
	      //disturbCnt++;
	    }
	  }
	}
	
	//Edge case 3: This zero is somewhere in the middle of the word
	else{
	  //---Look to the right and left for a 0---
	  if( ( (dataPair[1][wordIndex] & (1ULL<<(bitIndex-1))) == 0 )||((dataPair[1][wordIndex] & (1ULL<<(bitIndex+1))) == 0)){
	    indirectUpsCnt++;
	    //disturbCnt++;
	    //dstScoreBoard[wordIndex*numBits + ( (numBits-1) - bitIndex )]=1;    
	  }
	}
      }
    }
  }
  return indirectUpsCnt;//disturbCnt;
}

//Find the number of indirect errors in the dataPair update. An indirect error occurs when a cell transitions from 0->1 and there is no cell around it that is a 0 after th update. 
int tallyIndirectErrors(unsigned long long** dataPair,int* errorScoreBoard){
  int numWords = 8;//Each word is 8 bytes, and there's 8 words per cache block.
  int numBits = 64;//Bits in each word
  int errorCnt=0;
  int errorBoardIndex=0;
  //Start from the rightmost word in the data pair, as well as the rightmost bit
  for(int wordIndex=numWords-1; wordIndex>=0;wordIndex--){
    for(int bitIndex=0; bitIndex<numBits;bitIndex++){
      //Look for a 0/reset cell that is a zero in dataPair[0] and one in dataPair[1]
      if( ( (dataPair[0][wordIndex] & (1ULL << bitIndex)) == 0 ) && ( (dataPair[1][wordIndex] & (1ULL << bitIndex)) != 0 ) ){
	errorBoardIndex=wordIndex*numBits+( (numBits-1)-bitIndex); 
	//Edge case 1: This zero is the rightmost bit in the word
	if(bitIndex==0){
	  if(wordIndex == numWords-1){
	    //---Edge case 1.1: This word is the rightmost word in the data
	    //No disturb from the right possible
	
	    //---Look to the left for a 0---
	    //Check if bit to the left is a 0 in the update
	    if( (dataPair[1][wordIndex]) & (1ULL<< (bitIndex+1) ) == 0 ){     
	      //Do nothing, there's no error
	    }
	    else{
	      //Check the board to see if there's an error there already and record the error if not.  
	      if( errorScoreBoard[errorBoardIndex]==0 ){
		errorScoreBoard[errorBoardIndex]=1;
		errorCnt++;
	      }
	    }
	  }

	  //---Edge case 1.2: This is word is not rightmost word in data
	  else{
	    //---Look to the right and to the left for disturb--
	    //Check for adjecent 0 in dataPair[1]
	    if( ((dataPair[1][wordIndex+1]&(1ULL<<(numBits-1))) == 0 )||( (dataPair[1][wordIndex]) & (1ULL<< (bitIndex+1) ) == 0) ){	     
	      //Do nothing, there's no error
	    }	
	    else{
	      //Check the board to see if there's an error there already and record the error if not.  
	      if( errorScoreBoard[errorBoardIndex]==0 ){
		errorScoreBoard[errorBoardIndex]=1;
		errorCnt++;
	      }		
	    }
	  }				
	}		
	    
	    //Edge case 2: This zero is the leftmost bit in the word
	else if(bitIndex==numBits-1){
	  //Edge case 2.1: Word is leftmost word in data
	  if(wordIndex==0){
	    //No disturbance from the left possible
	    
	    //---Look to the right for a 0---
	    if( (dataPair[1][wordIndex] & (1ULL<<(numBits-2))) == 0 ){   
	      //Do nothing, there's no error
	    }	
	    else{
	      //Check the board to see if there's an error there already and record the error if not.  
	      if( errorScoreBoard[errorBoardIndex]==0 ){
		errorScoreBoard[errorBoardIndex]=1;
		errorCnt++;
	      }			
	    }	
	  }	
	  //Edge case 2.2: Word is not leftmost word in data
	  else{
	    //---Look to the right and left for a 0---
	    if( ( (dataPair[1][wordIndex] & (1ULL<<(numBits-2))) == 0 )||( (dataPair[1][wordIndex-1]& (1ULL)) == 0 ) ){
	      //Do nothing, there's no errod
	    }		
	    else{
	      //Check the board to see if there's an error there already and record the error if not.  
	      if( errorScoreBoard[errorBoardIndex]==0 ){
		errorScoreBoard[errorBoardIndex]=1;
		errorCnt++;
	      }		
	    }
	  }
	}
	
	//Edge case 3: This zero is somewhere in the middle of the word
	else{
	  //---Look to the right and left for a 0---
	  if( ( (dataPair[1][wordIndex] & (1ULL<<(bitIndex-1))) == 0 )||((dataPair[1][wordIndex] & (1ULL<<(bitIndex+1))) == 0)){
	      //Do nothing, there's no error
	  }		
	  else{
	    //Check the board to see if there's an error there already and record the error if not.  
	    if( errorScoreBoard[errorBoardIndex]==0 ){
	      errorScoreBoard[errorBoardIndex]=1;
	      errorCnt++;
	    }		
	  }
	}
      }
    }
  }	
  return errorCnt;
}

//Find the number of disturbed errors for this address from the score board, counting out the natural fixes. Natural fixes are errors that are fixed naturally by an update reseting a cell with a 1 to a cell with a 0. 
int tallyAddressDisturb(unsigned long long int* newData, int* dstScoreBoard, int boardSize){
  int dstCnt=0;
  int dataWordIndex; //newData comes in an array of 8 byte words so we have to translate between i, the index of the scoreBoard, and the index to access the bit in newData.
  int dataWordBitIndex;
  int numBitsPerWord=64;
  //long int naturalFixCnt;

  for(int i=0;i<boardSize;i++){
    if(dstScoreBoard[i]==1){
      dataWordIndex=floor( ((double) i/64) ); 
      dataWordBitIndex= (numBitsPerWord-1) - i%numBitsPerWord; 
      //Check if the bit at position i is still disturbed, that is, that it's still a 0 in the update 
      if( (newData[dataWordIndex] & (1ULL<<dataWordBitIndex)) == 0 ){
	//Still disturbed
	dstCnt++;
	//printf("This is i=%d, the wordIndex=%d and bitIndex=%d for disturbed bit i \n", i, dataWordIndex, dataWordBitIndex);
      }
      else{
	//No longer disturbed: Reset scoreBoard at index i, and increase naturalFixCntd
	dstScoreBoard[i]=0;
	naturalFixCnt++;//Global Variable. 
      }
    }
  }
  return dstCnt;
}


//Find the number of indirect errors at this address from the score board and the data. We need the data because there may be natural fixes in the data that the scoreboard does not reflect. 
int tallyAddressIndErrors(unsigned long long int* newData, int* dstScoreBoard, int boardSize){
  int indErrCnt=0;//indirect error counter
  int dataWordIndex; //newData comes in an array of 8 byte words so we have to translate between i, the index of the scoreBoard, and the index to access the bit in newData.
  int dataWordBitIndex;
  int numBitsPerWord=64;
  //long int naturalFixCnt;

  for(int i=0;i<boardSize;i++){
    if(dstScoreBoard[i]==1){
      dataWordIndex=floor( ((double) i/64) ); 
      dataWordBitIndex= (numBitsPerWord-1) - i%numBitsPerWord; 
      //Check if the bit at position i is still in error, that is, it's still a 1. 
      if( (newData[dataWordIndex] & (1ULL<<dataWordBitIndex)) != 0 ){
	//Still disturbed
	indErrCnt++;
	//printf("This is i=%d, the wordIndex=%d and bitIndex=%d for disturbed bit i \n", i, dataWordIndex, dataWordBitIndex);
      }
      else{
	//No longer disturbed: Reset scoreBoard at index i, and increase naturalFixCntd
	dstScoreBoard[i]=0;
	naturalFixCnt++;//Global Variable. 
      }
    }
  }
  return indErrCnt;
}

//Tally the number of indirect errors at an address
//-----------------------------------------------------------------------
//---------------------BitFlipCnt Helper functions-----------------------
//-----------------------------------------------------------------------

//Split a string into a supplied vector. Inspired by: 
//http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
  std::stringstream ss(s);
  std::string item;
  elems.clear();
  while (getline(ss, item, delim)) {
    if(item == "")
      continue;
    elems.push_back(item);
  }
  return elems;
}


//Get a pair of data from the DRAMtrace
bool getDataPair(unsigned long long** dataPair, int pairCnt){
  std::string line1;
  std::string line2;
  std::vector<std::string> datAddr1; //data and address vector
  std::vector<std::string> datAddr2;
  int addrIndex = 9; //From how the line is parsed in sorted trace. 
  int lineCnt; 
  long long lineNum=0;
  unsigned long long byts1;
  unsigned long long byts2;
  unsigned long long tempAddr; 
  
  //Process first two lines
  if(pairCnt==0){
    //Get addresses 2 vectors of data and addresses. 
    getline(DRAMtrace,line1);
    getline(DRAMtrace,line2);
    datAddr1 = split(line1 ,' ',datAddr1);
    datAddr2 = split(line2 ,' ',datAddr2);

    addr1 = stoull(datAddr1[addrIndex],NULL,16);
    addr2 = stoull(datAddr2[addrIndex],NULL,16);
    
    //Copy data to pair of data
    for(int i=1;i<addrIndex;i++){ //i is 1 to skip the silent write counter in the line
      byts1 = stoull(datAddr1[i],NULL,16);
      byts2 = stoull(datAddr2[i],NULL,16);
      memcpy( (void*)&dataPair[0][i-1],(void*)&byts1,8 );
      memcpy( (void*)&dataPair[1][i-1],(void*)&byts2,8 );
    }

    return true;
  }
  //Process lines after first pair of lines
  else{
    //Check if there's lines left
    if( getline(DRAMtrace,line2) ){
      //Get next address to compare to addr2
      datAddr2 = split(line2 ,' ',datAddr2);
      tempAddr = stoull(datAddr2[addrIndex],NULL,16);
      if(tempAddr == addr2){
	addr1 = addr2; 
	addr2 = tempAddr; 
	memcpy( (void*)dataPair[0],(void*)dataPair[1],64 );
	
	for(int i=1;i<addrIndex;i++){
	  byts2= stoull(datAddr2[i],NULL,16); 
	  memcpy( (void*)&dataPair[1][i-1],(void*)&byts2,8 ); 
	}
	return true; 
      }
      //Addresses are different => new set of updates
      else{
	getline(DRAMtrace,line2); 
	datAddr1 = datAddr2; 
	datAddr2 = split(line2 ,' ',datAddr2);
	addr1 = tempAddr; 
	addr2 = stoull(datAddr2[addrIndex],NULL,16);
	//Copy data to pair of data                                                               
	for(int i=1;i<addrIndex;i++){ //i is 1 to skip the silent write counter in the line
	  byts1 = stoull(datAddr1[i],NULL,16);
	  byts2 = stoull(datAddr2[i],NULL,16);
	  memcpy( (void*)&dataPair[0][i-1],(void*)&byts1,8 );
	  memcpy( (void*)&dataPair[1][i-1],(void*)&byts2,8 );
	}
	return true; 
      }
    }
    //There's no lines left
    else{ 
      return false; 
    }
  }  
  return true; 

}

//Get ECC bits from the input data word of size 64 bits.
int getWordECC(unsigned long long data){
  //Save the arrays with the right indexes within the data to xor for each check bit P0-P6
  int P0inxs[35]={
    0,1,3,4,6,8,10,11,13,15,17,19,21,23,25,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,57,59,61,63
  };
  int P1inxs[35]={
    0,2,3,5,6,9,10,12,13,16,17,20,21,24,25,27,28,31,32,35,36,39,40,43,44,47,48,51,52,55,56,58,59,62,63
  };
  int P2inxs[35]={
    1,2,3,7,8,9,10,14,15,16,17,22,23,24,25,29,30,31,32,37,38,39,40,45,46,47,48,53,54,55,56,60,61,62,63
  };
  int P3inxs[31]={
    4,5,6,7,8,9,10,18,19,20,21,22,23,24,25,33,34,35,36,37,38,39,40,49,50,51,52,53,54,55,56
  };
  int P4inxs[33]={
    11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
  };
  int P5inxs[33]={
    26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
  };
  int P6inxs[7]={
    57,58,59,60,61,62,63
  };
  static int P[8];
  int *Pinxs[7]={
    P0inxs, P1inxs, P2inxs, P3inxs, P4inxs, P5inxs, P6inxs
  }; 
  int PinxsSize[7]={
    35,35,35,31,33,33,7
  };

  //---------------------Get P0-P6-------------------
  int i;
  int j; 
  //unsigned long int data = strtoul(argv[1],NULL,16); 
  for(j = 0; j< LEN(P)-1; j++){ 
    P[j] = (data>>Pinxs[j][0])%2; 
    for(i = 1; i<PinxsSize[j]; i++){
      P[j] = P[j] ^ ((data>>Pinxs[j][i])%2);
    }
  }
  //---------------------Get P7----------------------
  //XOR P7 with all the data bits
  P[7] = data%2;
  for(i=1; i<64; i++){
    P[7]=P[7] ^ ((data>>i)%2);
  }
  //XOR P7 with all the check bits
  for(i=0; i<7; i++){
    P[7]=P[7] ^ P[i];
  }
  
  //Transform P into a string
  std::stringstream ss; 
  for(int k: P)
    ss << k;
  //Transform P into an int
  int result; 
  result = std::stoi(ss.str().c_str(), NULL, 2); 
  
  return result;
}


//Get Average bitflips between two pairs of data at the block granularity
float getAvgDfsBG(unsigned long long** pair){
  unsigned long long c; 
  int j=0; 
  float avg=0; 
  float cnt=0;
  int numIters=sizeof(unsigned long long)*8;
  int numWords=8; 
  //char cStr[sizeof(unsigned long long)*8+1]; 
  for(int i=0;i<numWords;i++){
    c = pair[0][i]^pair[1][i];
    //Count number of 1's/bitflips in c
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) != 0){
	cnt++; 
      }
    }
    avg+=cnt/numIters;
    cnt=0; 
  }
  return avg/=(numWords); 
}

//Get Average bitflips between two pairs of ECC at the block granularity
float getAvgECCfsBG(int** pair){
  unsigned int c; 
  int j=0; 
  float avg=0; 
  float cnt=0;
  int numIters=8;
  int numWords=8; 
  //char cStr[sizeof(unsigned long long)*8+1]; 
  for(int i=0;i<numWords;i++){
    c = pair[0][i]^pair[1][i];
    //printf("Pairs and c: %#x, %#x, %#x\n",pair[0][i],pair[1][i],c);
    //Count number of 1's/bitflips in c
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) != 0){
	cnt++; 
      }
    }
    avg+=cnt/numIters; 
    cnt = 0; 
  }
  return avg/=(numWords); 
}


//Get Average bitflips between two pairs of data at the word granularity
float getAvgDfsWG(unsigned long long** pair){
  unsigned long long c; 
  int j=0; 
  int silentWords=0;
  float avg=0; 
  float cnt=0;
  int numIters=sizeof(unsigned long long)*8;
  int numWords=8; 
  //char cStr[sizeof(unsigned long long)*8+1]; 
  for(int i=0;i<numWords;i++){
    c = pair[0][i]^pair[1][i];
    //Count number of 1's/bitflips in c
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) != 0){
	cnt++; 
      }
    }
    //If cnt==0 we got a silent word so we count it to subtract it from the numWords. 
    if(cnt==0){
      silentWords++;
    }
    avg+=cnt/numIters;
    cnt=0; 
  }
  wordCnt+= numWords-silentWords; 
  return avg;// /=(numWords-silentWords); 
}

//Get Average bitflips between two pairs of ECC at the word granularity
float getAvgECCfsWG(int** pair){
  unsigned int c; 
  int j=0; 
  int silentWords=0;
  float avg=0; 
  float cnt=0;
  int numIters=8;
  int numWords=8; 
  //char cStr[sizeof(unsigned long long)*8+1]; 
  for(int i=0;i<numWords;i++){
    c = pair[0][i]^pair[1][i];
    //printf("Pairs and c: %#x, %#x, %#x\n",pair[0][i],pair[1][i],c);
    //Count number of 1's/bitflips in c
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) != 0){
	cnt++; 
      }
    }
    //If cnt==0 we got a silent word so we count it to subtract it from the numWords.                              
    if(cnt==0){
      silentWords++;
    }
    avg+=cnt/numIters; 
    cnt = 0; 
  }
  eccWordCnt+=(numWords-silentWords);
  //Multiple data words can have the same codeword, in this case there are no bit flips for that word
  //and if the other words in the 64byte block are also the same, the avg of bit flips is 0. 
  if(numWords==silentWords){
    return 0; 
  }
  return avg;// /=(numWords-silentWords); 
}
