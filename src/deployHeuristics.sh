#!/bin/bash                                                                                                                
#Edited by Alfredo J. Velasco 8/4/15                                                                                       
#Project: DEPD                                                                                                             
#Script Description: Commands to produce the metrics/figures of merit for DEPD project                                    
#specifically, heuristics results. 
#Input: [MaxNumberOfProtectedECCbits]

cnt=1
maxEccProtection=$1
#eccProtectionDir=$2

#mkdir ~/repos/depd/resrcs/$eccProtectionDir/

echo $maxEccProtection
for j in $( ls ~/repos/depd/src/bin/impHeuristic* ); do
    for i in $( ls /usr/xtmp/alfredo/pintoolResults/sorted_*_native.txt ); do
	echo -----------------------Running $j on: $i ---------------------------------
	
	(time $j $i $maxEccProtection > $i\H$cnt\Ecc$maxEccProtection\Results; mv $i\H$cnt\Ecc$maxEccProtection\Results ~/repos/depd/resrcs/heurisResults) &
	echo ---------------------------------------------------------------------------
    done
    cnt=$((cnt+1))
done
