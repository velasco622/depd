/*
Edited by Alfredo J. Velasco 7/20/2015 
For project: DRAM-ECC PCM-Data
Description: This file contains the logic to run a cache simulator. It all works with VirtIns strucs that can be made from a trace file or from anything really. A function that makes the VirtIns 
objects can be made however, and the VirtIns can be put through the cache to get realistic eviction
s etc with LRU policy. 
Simulation flow: A memory access is read from somewhere and placed within the cache. Data is
kept null. Whenever a block is evicted due to a capacity conflict, the block
is checked if it's dirty or clean and is writen back if dirty. 

--------------------------------THIS MIGHT NOT BE TRUE AFTER COMBINING THIS TO PINTOOL-------------
The write backs are recorded in an address histogram stored in an unordered map. The address
histogram contains all the writes that would be made to DRAM, and is writen out
to a file called DRAMtrace.out
-------------------------------------------------------------------------------------------------
*/

#include "CacheSim.h" 
//----------------------------------------------------------------------------
//------------------------------Helper Functions------------------------------
//----------------------------------------------------------------------------

//This function initializes our cache's blocks
struct Cache* initCache(long int size, int ways){
  //Create blocks
  int sets = (size/64)/ways;
  struct Cache* newCache = new struct Cache;
  CacheBlock** newCacheBlocks = new CacheBlock*[sets];  
  for(int i=0; i<sets;i++){
    newCacheBlocks[i] = new CacheBlock[ways];
    //Go through the set i's ways to initialize the cache blocks
    for(int j=0; j<ways;j++){
      newCacheBlocks[i][j].isValid=false; 
      newCacheBlocks[i][j].isDirty=false; 
    } 
  }
  
  /*
  //Test the cache
  for(i=0;i<sets;i++){
    for(j=0;j<ways;j++){
      printf("This is cache block (%d,%d), address:%p:\n",i,j, (void*)&newCacheBlocks[i][j]); 
      printf("isDirty:%d, isOpen:%d,data:%p\n",newCacheBlocks[i][j].isDirty, newCacheBlocks[i][j].isOpen,(void*)newCacheBlocks[i][j].data); 
    }
  }
  */
  //Point cache to those blocks
  newCache->cacheBlocks = newCacheBlocks;
  newCache->numWays = ways; 
  newCache->size = size; 
  //newCache->level = 1; //Change this to user input later!!!
  return newCache; 
}

//Read an operation from the virtual trace to put it through the cache. 
bool getOp(struct VirtIns* op, std::ifstream* vTrace){
  std::string line; 
  //printf("Line Count: %llu",lineCnt); 
  //string::size_type sz = 0; 
  //Read the first line, i.e. R or W and check for file status
  if(getline(*vTrace, line)){
    lineCnt++; 
    //check if its a read
    if(line.compare("W")==0)
      op->isWrite=true; 
    else op->isWrite=false;
    memOps++; 
    //Read the second address and save it to op
    getline(*vTrace, line); //useless read
    getline(*vTrace, line); //Read's aligned address
    lineCnt++;
    lineCnt++;							
    if(line.compare("")==0) getline(*vTrace,line); //Inserted this for a weird extra new line at line 85,330,275 in ..../resrcs/DEPDtrace_blackscholes_simsmallLaggedData.out
    if(line.compare("")==0) return false; //At the end of the trace we get two "" characters in a row, so we can exit then. 
    op->addr = stoull(line,NULL,0);
    /* Data is not copied anymore make sure traces you read from don't contain data
    //Copy data if it's write
    if(op->isWrite){
      for(int i=0; i<8; i++){
	getline(*vTrace, line);
	lineCnt++;
	if(line.compare("(nil)")==0)
	  op->data[i]=0; 
	else op->data[i] = stoull(line,NULL,0);
      }
    }
    */
    getline(*vTrace, line);//useless read to throw out empty new line before next instruction
    //getline call above makes the next getline call pick up the next new instruction. 
    //cout<<line<<"\n";  
    lineCnt++;
    return true; 
  } 
  else{
    //vTrace is broken or we reached the end
    return false; 
  } 
}

//Systematically print out the attributes of a virtual instruction "op"
void printOp(struct VirtIns* op){
  printf("Printing an Op:\n"); 
  printf("op->isWrite: %d\n", op->isWrite); 
  printf("op->addr: %#llx\n", op->addr); 
  printf("op->data:\n");
  /*No more data
  if(op->data){
    for(int i=0;i<8;i++){
      printf("%#llx\n",op->data[i]); 
    }
  }
  //We only add data to op when the write is writen back. 
  else printf("Data hasn't been added to op\n"); 
  */
}

//Get the tag for the cache from the 64 byte aligned address in op
long long unsigned getTag(struct VirtIns* op, int size, int ways){
  long long unsigned tag = op->addr >> (int)(6+log2((size/64)/ways));
  //printf("this is address: %#llx\n", op->addr); 
  return tag; 
}

//Get the cache offset to index into the cache
long long unsigned getInx(struct VirtIns* op, int size, int ways){
  int blockOsetBits = 6;
  int addrSize = 64; //addSize is in bits, but This is also the size of our cache block conviniently in bytes
  long double cacheOsetBits = log2( (size/addrSize)/ways ); 
  //If the cache is 1 block in size, the index is always 1. 
  if(cacheOsetBits==0){
    return 0; 
  }
  int numTagBits = addrSize - (int)cacheOsetBits - blockOsetBits;
  //printf("This is numTagBits: %d\n", numTagBits);
  //printf("This is cacheOsetBits: %d\n",(int)cacheOsetBits); 
  long long unsigned inx = op->addr << numTagBits; 
  //printf("This is inx after we shift left by numTagBits: %#llx",inx); 
  inx = inx >> (numTagBits+blockOsetBits); 
  return inx; 
}


//LRU is implemented by making the set a FIFO. The first block in the set is the most recently updated block, and the last one is the LRU block. updateBlock(), getBlockToEvict(), and checkForInvalidWays() are edited to set up this behavior. 
//Copy all the values and leave the source block invalid and clean
void copyBlocks(CacheBlock* srcBlock,CacheBlock* dstBlock){
  dstBlock->isValid=srcBlock->isValid;
  dstBlock->isDirty=srcBlock->isDirty; 
  dstBlock->addr   =srcBlock->addr;  
  dstBlock->tag    =srcBlock->tag; 
  //dstBlock->data   =srcBlock->data; 
  //Set up srcBlock to be invalid
  srcBlock->isValid=false; 
  srcBlock->isDirty=false; 
}

//Check if the block you need is in the cache
CacheBlock* checkForBlock(long long unsigned opTag, long long unsigned cacheInx, struct Cache* L1cache){
  int resBlockInx; 
  CacheBlock* curBlock;
  CacheBlock tempBlock;
  CacheBlock* lastBlock;
  CacheBlock* prevLastBlock;
  //compare tags with all the ways in the cache
  for(int i=0; i < L1cache->numWays; i++){
    //index into cache and way
    curBlock = &L1cache->cacheBlocks[cacheInx][i];
    //Check if block not(isValid)
    if(!curBlock->isValid){
      continue; 
    }
    else{
      //compare tags 
      if(curBlock->tag == opTag){
	//Save resident block in temp to maintain the set as an LRU FIFO
	copyBlocks(curBlock,&tempBlock); 
	resBlockInx = i; 
	//Cascade blocks' contents to fill the spot of the resident block
	for(int j=resBlockInx;j>0;j--){
	  lastBlock = &L1cache->cacheBlocks[cacheInx][j];
	  prevLastBlock = &L1cache->cacheBlocks[cacheInx][j-1];
	  copyBlocks(prevLastBlock,lastBlock);
	}
	//Return first block in set to be possibly writen back and updated
	curBlock = &L1cache->cacheBlocks[cacheInx][0];
	copyBlocks(&tempBlock,curBlock); 
	return curBlock; 
      }
    }
  }
  return NULL; 
}

//Check for open ways in a set
CacheBlock* checkForInvalidWays(long long unsigned opTag, long long unsigned cacheInx, struct Cache* L1cache){
  CacheBlock* curBlock;
  CacheBlock* lastBlock;
  CacheBlock* prevLastBlock; 
  curBlock = &L1cache->cacheBlocks[cacheInx][L1cache->numWays-1]; 
  //Check if last block in set is valid
  if(curBlock->isValid){
    //This means all blocks in set are valid as well
    return NULL; 
  }
  //There's at least one invalid block in set
  else{
    //Check if first block is invalid to return it right away, also means all blocks are invalid in set
    curBlock = &L1cache->cacheBlocks[cacheInx][0];
    if(!curBlock->isValid){
      return curBlock; 
    }
    //Shift the values of the blocks in the set by one towards the end to return the first block
    //in set, and keep LRU order.
    else{ 
      //Waterfall the values of the valid blocks from start of the set (low indexes) to the end of the set (higher indexes)
      for(int i=L1cache->numWays-1;i>0;i--){
	lastBlock = &L1cache->cacheBlocks[cacheInx][i];
	prevLastBlock = &L1cache->cacheBlocks[cacheInx][i-1];
	copyBlocks(prevLastBlock,lastBlock); 
      }
      //Return first block in set to be possibly writen back and updated
      curBlock = &L1cache->cacheBlocks[cacheInx][0]; 
      return curBlock; 
    }
  } 
}

//update the cache block and all that good stuff. 
void updateBlock(long long unsigned opTag, long long unsigned opCacheInx, struct VirtIns* op, CacheBlock* block, struct Cache* cache){
  //printf("We're updating block with tag: %#llx from op: \n",opTag); 
  //printOp(op); 
  //Maintain the FIFO for evictions by checking if block is at front of set (index 0)
  CacheBlock* frontBlock = &cache->cacheBlocks[opCacheInx][0]; 
  CacheBlock tempBlock; 
  CacheBlock* lastBlock;
  CacheBlock* prevLastBlock;
  
  //If the block we're updating 'block' is not the front block we know it's because 'block' comes from an eviction, and we can shift all the values down the set to place 'block' in front
  if(frontBlock->tag!=block->tag){
    copyBlocks(block,&tempBlock); 
    //Waterfall the values of the valid blocks from start of the set (low indexes) to the end of the set (higher indexes)
    for(int i=cache->numWays-1;i>0;i--){
      lastBlock = &cache->cacheBlocks[opCacheInx][i];
      prevLastBlock = &cache->cacheBlocks[opCacheInx][i-1];
      copyBlocks(prevLastBlock,lastBlock); 
    }			
    //Save values back to the front block
    copyBlocks(&tempBlock,frontBlock); 
    //Make 'block' point to the front block 
    block = frontBlock; 
  }
  
  //Check if read or write
  if(op->isWrite){
    block->isValid= true; 
    block->isDirty= true; 
    block->addr   = op->addr; 
    block->tag    = opTag; 
    //memcpy((void*)block->data, (void*)op->data, 64); 
  }
  else{
    block->isValid= true; 
    block->addr   = op->addr;
    block->tag    = opTag; 
  }
}

//Find a block in the cache to evict, and return a pointer to it  
CacheBlock* getBlockToEvict(long long unsigned opCacheInx, struct Cache* L1cache){
  //Return the last block in the set FIFO. When this function is called we know all blocks are
  //valid in the set, and the last one is the LRU block because of how blocks are shuffled when a set is accessed with checkForInvalidWays() and checkForBlock()
  return &L1cache->cacheBlocks[opCacheInx][L1cache->numWays-1]; 
}  

//Function updates a map of all the updates to DRAM to print them out later in order, use after a block is evicted from the cache and you get data with PIN_SafeCopy()
void updateDRAMhist(long long unsigned int addr/*CacheBlock* block*/,long long unsigned int* data){
  //writeBacks++; 
  //make the data update from cache block's attributes
  DRAMupdate* dUp = new DRAMupdate;
  dUp->data = new long long unsigned int[8]; 
  //DRAMupdate testDup; 
  memcpy((void*)dUp->data,(void*)data,64);
  
  //Check if there's a value at the key address
  auto t = DRAMhist.find(addr); 
  if(t == DRAMhist.end()){  //testDup==NULL){
    dUp->nextUpdate = dUp; 
    dUp->lastUpdate = dUp; 
    DRAMhist[addr]=dUp; 
  }
  else{
    (t->second->lastUpdate)->nextUpdate = dUp; 
    t->second->lastUpdate=dUp; 
  }
}


//Write back the input block (This allows us to print out the DRAM addresses and their writes in the order in which they are made)
void cbWriteBack(CacheBlock* block/*,long long unsigned int* data*/){
  writeBacks++;
  //updateDRAMhist(block->addr, data); 
  //Reset blocks attributes
  block->isValid = false;
  block->isDirty = false; 
  //block->data    = NULL; 
}

//Print data to an input file and end it with a new line character
void fprintData(FILE* file, long long unsigned int* data){
  for(int i=0; i<8; i++){
    fprintf(file, "%#llx\n",data[i]); 
  }
  fprintf(file, "\n"); 
}

//Print the histogram of address accesses and data to a file for later processing. 
void writeDRAMhist(char* traceFileName){
  FILE* DRAMtrace; 
  DRAMupdate* curUp; 
  DRAMtrace = fopen(traceFileName, "w");
  //Iterate through keys
  for(auto const &entry : DRAMhist){
    fprintf(DRAMtrace, "Address: %#llx\n", entry.first); 
    fprintf(DRAMtrace, "Data: \n"); 
    curUp = entry.second; 
    //Edge case 1: linked list is 1 in size
    if(curUp==(entry.second)->lastUpdate){
      fprintData(DRAMtrace,curUp->data); 
    }
    //Edge case 2: Print first, middle and last elements in linked list
    else{
      //entry.second points to the beginning of a linked list of data updates
      //for each key, print out linked list of data updates to same key/address
      while(curUp != (entry.second)->lastUpdate){
	fprintData(DRAMtrace, curUp->data); 
	curUp = curUp->nextUpdate; 
      }
      //Print last last node in list that broke the while loop
      fprintData(DRAMtrace,curUp->data); 
    }
  }
  fclose(DRAMtrace); 
}


//remove memory allocated for the DRAMupdates
void cleanUpDRAMupdates(){
  DRAMupdate* cur; 
  DRAMupdate* next; 
  //Iterate through keys
  for(auto const &entry : DRAMhist){
    cur = entry.second;
    //Edge case 1: linked list is 1 in size
    if(cur==(entry.second)->lastUpdate){
      delete [] cur->data; 
      delete cur;  
    }
    //Edge case 2: Print first, middle and last elements in linked list
    else{
      //entry.second points to the beginning of a linked list of data updates
      //for each key, print out linked list of data updates to same key/address
      cur=entry.second->nextUpdate; 
      while(cur != (entry.second)->lastUpdate){
	//fprintData(DRAMtrace, curUp->data); 
	next = cur->nextUpdate; 
	delete [] cur->data; 
	delete cur;
	cur = next; 
      }
      //Print last last node in list that broke the while loop
      //fprintData(DRAMtrace,curUp->data); 
      delete [] entry.second->data; 
      delete entry.second; 
      delete [] cur->data;
      delete cur; 
    }
  }
} 

//Remove memory allocated for the cache.
void cleanUpCache(struct Cache* cache){
  int sets = (cache->size/64)/cache->numWays;
  for(int i=0; i< sets; i++){
    /*no more data in blocks
    for(int j=0; j<cache->numWays; j++){
      delete [] (cache->cacheBlocks[i][j]).data; 
      //delete cache->cacheBlocks[i][j]; 
    }
    */
    delete [] cache->cacheBlocks[i]; 
  }
  delete cache; 
} 

//----------------------------------------------------------------------------
//------------------------------Main cache access logic-----------------------
//----------------------------------------------------------------------------
long long unsigned int* accessCacheSim(/*int size, int ways,*/ struct VirtIns* op, struct Cache* cache/*int argc, char* argv[]*/){
  //Local vars 
  bool isPresent;    //is block present in the cache?
  bool eofReached;   //to check if we got to the end of the virtual trace file
  long long unsigned int opTag; 
  long long unsigned int opCacheInx;
  static long long unsigned int victimAddr; //Declared static to enable the return of a pointer to it.
  //struct Cache* L1cache; //Can make this an array of caches later
  int size= cache->size; //atoi(argv[1]); 
  int ways= cache->numWays;//atoi(argv[2]);
  //virtTrace.open(argv[3]);
  //Char DRAMtraceOutFile[100] = "DRAMtraceFile.txt";//argv[4]; 
  //long long int* blockData;
  CacheBlock* block;
  //struct VirtIns* op = new VirtIns;
 

  //initiallize the block structs in the cache
  //L1cache = initCache(size, ways); 
  
  //New operation/instruction from virtual trace
  //while(getOp(op, &virtTrace)){ 
  //Get cache index and tag (to put the new memory operation into the cache)
  opTag = getTag(op, size, ways); 
  opCacheInx = getInx(op, size, ways);
  //printf("tag: %llu\n",opTag); 
  //printf("index: %llu\n",opCacheInx); 
  
  //Check if block is already in cache
  
  //printOp(op); //DB
  
  block = checkForBlock(opTag, opCacheInx, cache); 
  //block is present (block points to present block or NULL if not present.)
  if(block){
    updateBlock(opTag,  opCacheInx, op, block,cache); 
  }
  //block not present (cache miss)
  else{
    //Check the miss that just happened
    if(op->isWrite) writeMisses++; 
    else readMisses++; 
    //Print miss
    //printf("Just missed on tag: %llx\n",opTag); 
    
    //Check for invalid ways (in the indexed cache set)
    block = checkForInvalidWays(opTag, opCacheInx, cache); 
    //Invalid ways (block points to an invalid block if there's an invalid way, otherwise block points to NULL)
    if(block){
      updateBlock(opTag,  opCacheInx, op, block,cache); 
    }
    //no open ways (block = null)
    else{
      //Evict a block (to save new op into it)
      block = getBlockToEvict(opCacheInx, cache);//Returns pointer to an evicted block	
      //Check if block is dirty (so we can write it back if it is)
      if(block->isDirty){
	cbWriteBack(block); //Reset isValid and isDirty
	//return &block->addr; 
	//Reset blocks attributes
	//block->isValid = false;
	//block->isDirty = false;
      }
      //Save return address before we update
      victimAddr=block->addr; 
      //Update the block with new update (regardless of block state)
      updateBlock(opTag, opCacheInx, op, block,cache);
      return &victimAddr;//&block->addr; 
    }
  } 
  //}
  
  //writeDRAMhist(DRAMtraceOutFile); Call this at the end of Pintool!  
  //----------Use with CacheTest4-----------
  //Check that every block in cache is invalid
  int invalidCnt=0; 
  for(int i=0;i<(cache->size/cache->numWays)/64;i++){
    for(int j=0;j<cache->numWays;j++){
      if(!cache->cacheBlocks[i][j].isValid){
	//printf("We found an invalid block: This is wrong!!!\n"); 
	invalidCnt++; 
      }
    }
  }
  printf("Invalid blocks found: %d\n",invalidCnt); 
  //-----------------------------------------
  printf("Memory Ops: %d\n", memOps); 
  printf("writebacks: %d\n", writeBacks);
  printf("Read Misses: %d\n", readMisses); 
  printf("Write Misses: %d\n", writeMisses);
  
  //Call these functions below outside of the cache when the program/pintool is over
  //Clean up memory
  //cleanUpDRAMupdates();
  //printf("Cleaned up DRAMupdates\n"); 
  //cleanUpCache(L1cache); 
  //printf("cleaned up cache"); 
  //delete op; 
  return NULL; 
}
