/*
Edited by Alfredo J. Velasco 7/19/2015 
For project: DRAM-ECC PCM-Data
Description: Writes a dummy file with memory requests that will exercise the 
cache's eviction logic to prove correct. Correct behavior involves not evicting
anything. That is, a properly working cache, will not evict anything when 
matched to the size and ways of the file produced here. 
Program flow: Make a series of write accesses to fill up all the ways in 
all the lines. Make write requests and then make read requests to 
the same addresses to show that everything stays in the cache. 
Inputs: size and ways of the cache to produce the test for
Outputs: Dummy file at this path: 
"/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest1.txt" 
with the name number of input writes with the following format:
 
R
[addr]
[aligned Addr]
\n
W
[addr]
[aligned addr]
\n
.
.
.
R
[addr]
[aligned addr] (no new line character after the last line of addr or data)
 */

#include "CacheTests.h"

int main(int argc, char* argv[]){
  //Set up
  FILE* outFile; 
  outFile = fopen("/home/alfredo/Documents/repos/depd/resrcs/testFiles/CacheTest1.txt", "w");
  int size = atoi(argv[1]); 
  int ways = atoi(argv[2]); 
 
  //print writes of 0's to every block to fill the cache
  pFillWrites(outFile,size,ways); 
  
  //update the same addresses to make them dirty
  pFillWrites(outFile,size,ways); 
  
  //Make read operations in order from lower to higher addresses, going through the tags to show there's no eviction. 
  pOrdReads(outFile, size,ways); 
  
  fclose(outFile); 
  return 0; 
}

