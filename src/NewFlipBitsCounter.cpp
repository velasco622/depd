/*
Edited by Alfredo J. Velasco 8/3/2015 
For project: DRAM-ECC PCM-Data
Description: This file is pretty much like FlipBitCounter.cpp, but it uses a different format to 
read the data and the addresses. Instead of address and rows of data, this program assumes the data
and the address to be in the same address. The data first in blocks of 8 bytes followed by the 
address. This file contains logic to read a DRAMtrace, a file with an address followed by the 
data writes to that address in successive order.  
Program flow: An address is read, and a pair data writes to the address is compared for the number
of bits that flipped. Then, the ECC bits are calculated on each write and the number of bit flips
between the ECC of one write is compared to the ECC bits of the other write in the pair. An aver
age is calculated for each pair, and an average is calculated for an address. The avarage bit flips
for all addresses is finally calculated for both ECC and data bits. 
Inputs: [PATHtoDRAMtraceFile]
Outpus: Message specifying the percentage of bits that flip between data writes, and the percentage
of bits that flip between different ECC bits of the corresponding writes: 

----------------------Sample output----------------------------------------------
Percentage of data bit flips: [SomeNumber]
Percentage of ECC bit  flips: [SomeNumber]
 */
#include <math.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <iostream>
#include <vector>
#include <sstream>

using namespace std; 

//---------------------Global Vars--------------------------------------
ifstream DRAMtrace; 
unsigned long long addr1; 
unsigned long long addr2; 
bool isFirstPair;

//-----------------------------------------------------------------------
//---------------------Helper functions----------------------------------
//-----------------------------------------------------------------------

//Split a string into a supplied vector. Inspired by: 
//http://stackoverflow.com/questions/236129/split-a-string-in-c
vector<string> &split(const string &s, char delim, vector<string> &elems) {
  stringstream ss(s);
  string item;
  elems.clear();
  while (getline(ss, item, delim)) {
    if(item == "")
      continue;
    elems.push_back(item);
  }
  return elems;
}


//Get a pair of data from the DRAMtrace
bool getDataPair(unsigned long long** dataPair, int pairCnt){
  string line1;
  string line2;
  vector<string> datAddr1; //data and address vector
  vector<string> datAddr2;
  //const string buf1;
  //const string buf2;
  int addrIndex = 9; //From how the line is parsed in sorted trace. 
  int lineCnt; 
  long long lineNum=0;
  unsigned long long byts1;
  unsigned long long byts2;
  unsigned long long tempAddr; 
  
  //Process first two lines
  if(pairCnt==0){
    //Get addresses 2 vectors of data and addresses. 
    getline(DRAMtrace,line1);
    getline(DRAMtrace,line2);
    datAddr1 = split(line1 ,' ',datAddr1);
    datAddr2 = split(line2 ,' ',datAddr2);

    addr1 = stoull(datAddr1[addrIndex],NULL,16);
    addr2 = stoull(datAddr2[addrIndex],NULL,16);
    
    //Copy data to pair of data
    for(int i=1;i<addrIndex;i++){ //i is 1 to skip the silent write counter in the line
      byts1 = stoull(datAddr1[i],NULL,16);
      byts2 = stoull(datAddr2[i],NULL,16);
      memcpy( (void*)&dataPair[0][i-1],(void*)&byts1,8 );
      memcpy( (void*)&dataPair[1][i-1],(void*)&byts2,8 );
    }

    return true;
  }
  //Process lines after first pair of lines
  else{
    //Check if there's lines left
    if( getline(DRAMtrace,line2) ){
      //Get next address to compare to addr2
      datAddr2 = split(line2 ,' ',datAddr2);
      tempAddr = stoull(datAddr2[addrIndex],NULL,16);
      if(tempAddr == addr2){
	addr1 = addr2; 
	addr2 = tempAddr; 
	memcpy( (void*)dataPair[0],(void*)dataPair[1],64 );
	
	for(int i=1;i<addrIndex;i++){
	  byts2= stoull(datAddr2[i],NULL,16); 
	  memcpy( (void*)&dataPair[1][i-1],(void*)&byts2,8 ); 
	}
	return true; 
      }
      //Addresses are different => new set of updates
      else{
	getline(DRAMtrace,line2); 
	datAddr1 = datAddr2; 
	datAddr2 = split(line2 ,' ',datAddr2);
	addr1 = tempAddr; 
	addr2 = stoull(datAddr2[addrIndex],NULL,16);
	//Copy data to pair of data                                                               
	for(int i=1;i<addrIndex;i++){ //i is 1 to skip the silent write counter in the line
	  byts1 = stoull(datAddr1[i],NULL,16);
	  byts2 = stoull(datAddr2[i],NULL,16);
	  memcpy( (void*)&dataPair[0][i-1],(void*)&byts1,8 );
	  memcpy( (void*)&dataPair[1][i-1],(void*)&byts2,8 );
	}
	return true; 
      }
    }
    //There's no lines left
    else{ 
      return false; 
    }
  }  
  return true; 

}

//Get ECC bits from the input data word of size 64 bits.
#define LEN(x)  (sizeof(x) / sizeof((x)[0]))
long int eccCnt=0; 

int getWordECC(unsigned long long data){
  //Save the arrays with the right indexes within the data to xor for each check bit P0-P6
  int P0inxs[35]={
    0,1,3,4,6,8,10,11,13,15,17,19,21,23,25,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,57,59,61,63
  };
  int P1inxs[35]={
    0,2,3,5,6,9,10,12,13,16,17,20,21,24,25,27,28,31,32,35,36,39,40,43,44,47,48,51,52,55,56,58,59,62,63
  };
  int P2inxs[35]={
    1,2,3,7,8,9,10,14,15,16,17,22,23,24,25,29,30,31,32,37,38,39,40,45,46,47,48,53,54,55,56,60,61,62,63
  };
  int P3inxs[31]={
    4,5,6,7,8,9,10,18,19,20,21,22,23,24,25,33,34,35,36,37,38,39,40,49,50,51,52,53,54,55,56
  };
  int P4inxs[33]={
    11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
  };
  int P5inxs[33]={
    26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
  };
  int P6inxs[7]={
    57,58,59,60,61,62,63
  };
  static int P[8];
  int *Pinxs[7]={
    P0inxs, P1inxs, P2inxs, P3inxs, P4inxs, P5inxs, P6inxs
  }; 
  int PinxsSize[7]={
    35,35,35,31,33,33,7
  };

  //---------------------Get P0-P6-------------------
  int i;
  int j; 
  //unsigned long int data = strtoul(argv[1],NULL,16); 
  for(j = 0; j< LEN(P)-1; j++){ 
    P[j] = (data>>Pinxs[j][0])%2; 
    for(i = 1; i<PinxsSize[j]; i++){
      P[j] = P[j] ^ ((data>>Pinxs[j][i])%2);
    }
  }
  //---------------------Get P7----------------------
  //XOR P7 with all the data bits
  P[7] = data%2;
  for(i=1; i<64; i++){
    P[7]=P[7] ^ ((data>>i)%2);
  }
  //XOR P7 with all the check bits
  for(i=0; i<7; i++){
    P[7]=P[7] ^ P[i];
  }
  
  //Transform P into a string
  stringstream ss; 
  for(int k: P)
    ss << k;
  //Transform P into an int
  int result; 
  result = stoi(ss.str().c_str(), NULL, 2); 
  
  return result;
}


//Get Average bitflips between two pairs of data
float getAvgDfs(unsigned long long** pair){
  unsigned long long c; 
  int j=0; 
  float avg=0; 
  float cnt=0;
  int numIters=sizeof(unsigned long long)*8;
  int numWords=8; 
  //char cStr[sizeof(unsigned long long)*8+1]; 
  for(int i=0;i<numWords;i++){
    c = pair[0][i]^pair[1][i];
    //Count number of 1's/bitflips in c
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) != 0){
	cnt++; 
      }
    }
    avg+=cnt/numIters;
    cnt=0; 
  }
  return avg/=(numWords); 
}

//Get Average bitflips between two pairs of ECC
float getAvgECCfs(int** pair){
  unsigned int c; 
  int j=0; 
  float avg=0; 
  float cnt=0;
  int numIters=8;
  int numWords=8; 
  //char cStr[sizeof(unsigned long long)*8+1]; 
  for(int i=0;i<numWords;i++){
    c = pair[0][i]^pair[1][i];
    //printf("Pairs and c: %#x, %#x, %#x\n",pair[0][i],pair[1][i],c);
    //Count number of 1's/bitflips in c
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) != 0){
	cnt++; 
      }
    }
    avg+=cnt/numIters; 
    cnt = 0; 
  }
  return avg/=(numWords); 
}


int main(int argc, char* argv[]){
  
  DRAMtrace.open(argv[1]);
  //Initialize structures to keep our data and ECC
  unsigned long long *dataPair[2];//= new unsigned long long*[2]; 
  float totDataAvg=0; 
  float totECCAvg=0; 
  int pairCnt = 0; 
  unsigned long long data1[8];//= new unsigned long long[8];  
  unsigned long long data2[8];//= new unsigned long long[8];
  int *ECCbitsPair[2];//= new int*[2];
  int ECC1[8];//=new int[8]; 
  int ECC2[8];//=new int[8]; 
  //int* isFirstPair; 
  ECCbitsPair[0]=ECC1; 
  ECCbitsPair[1]=ECC2; 
  dataPair[0]=data1; 
  dataPair[1]=data2; 
  
  
  //Get a pair of data to compare
  while(getDataPair(dataPair,pairCnt)){  
    //Get ECC bits
    for(int i=0;i<8;i++){
      ECC1[i]=getWordECC(dataPair[0][i]); 
      ECC2[i]=getWordECC(dataPair[1][i]);
      /*
      for(int k=0;k<8;k++){
	printf("ECC1: %x ECC2: %x\n", ECC1[k], ECC2[k]);
      } 
      */
    }
    //Get data pair average bitflips (We use void pointer to use the same function for both dataPair and ECCbitsPair)
    totDataAvg += getAvgDfs(dataPair); 
    //Get ECC bit pair average bitflips
    totECCAvg += getAvgECCfs(ECCbitsPair); 
    pairCnt++; 
  }
  totDataAvg/=pairCnt; 
  totECCAvg /=pairCnt;
  
  printf("Ratio of data bit flips: %lf\n", totDataAvg); 
  printf("Ratio of ECC bit  flips: %lf\n", totECCAvg);
  printf("Number of pairs counted: %d\n", pairCnt); 
 
  return 0; 
}
