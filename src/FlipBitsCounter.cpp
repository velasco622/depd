/*
Edited by Alfredo J. Velasco 7/17/2015 
For project: DRAM-ECC PCM-Data
Description: This file contains logic to read a DRAMtrace, a file with an address followed by the 
data writes to that address in successive order.  
Program flow: An address is read, and a pair data writes to the address is compared for the number
of bits that flipped. Then, the ECC bits are calculated on each write and the number of bit flips
between the ECC of one write is compared to the ECC bits of the other write in the pair. An aver
age is calculated for each pair, and an average is calculated for an address. The avarage bit flips
for all addresses is finally calculated for both ECC and data bits. 
Inputs: [PATHtoDRAMtraceFile]
Outpus: Message specifying the percentage of bits that flip between data writes, and the percentage
of bits that flip between different ECC bits of the corresponding writes: 

----------------------Sample output----------------------------------------------
Percentage of data bit flips: [SomeNumber]
Percentage of ECC bit  flips: [SomeNumber]
 */
#include <math.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <iostream>
using namespace std; 

//---------------------Global Vars--------------------------------------
ifstream DRAMtrace; 
bool isFirstPair;
//-----------------------------------------------------------------------
//---------------------Helper functions----------------------------------
//-----------------------------------------------------------------------

//Get a pair of data from the DRAMtrace
bool getDataPair(unsigned long long** dataPair){
  string line; 
  int lineCnt; 
  long long lineNum=0;
  unsigned long long byts;
  //Use to inform the main that we're counting the first pair
  //static int isFirstPair; 
  //isFpair = &isFirstPair; 
  if(getline(DRAMtrace, line)){
    if(line.empty()){
      getline(DRAMtrace,line); 
    }
    //We're at the beginning of an address
    if(line[0]=='A'){
      //Wasted line
      getline(DRAMtrace,line); 
      lineCnt = 0;
      //Initialize first pair to all 0's
      isFirstPair=true;
      for(int i = 0; i<8;i++){
	dataPair[0][i]=0; 
      }
    }else{
      isFirstPair=false;
      //line is the first piece of data
      lineCnt = 1;
      memcpy( (void*)dataPair[0], (void*)dataPair[1], 64); 
      byts = stoull(line,NULL,0); 
      memcpy( (void*)&dataPair[1][0], (void*)&byts, 8);  
    }
    //Copy data in in a while loop: 
    while(lineCnt<8){
      getline(DRAMtrace,line);
      byts = stoull(line,NULL,0);
      memcpy( (void*)&dataPair[1][lineCnt], (void*)&byts, 8);
      lineCnt++; 
    }
  }
  else{
    return false; 
  }
  return true; 
}

//Get ECC bits from the input data word of size 64 bits.
#define LEN(x)  (sizeof(x) / sizeof((x)[0]))
long int eccCnt=0; 

int getWordECC(unsigned long long data){
  //Save the arrays with the right indexes within the data to xor for each check bit P0-P6
  int P0inxs[35]={
    0,1,3,4,6,8,10,11,13,15,17,19,21,23,25,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,57,59,61,63
  };
  int P1inxs[35]={
    0,2,3,5,6,9,10,12,13,16,17,20,21,24,25,27,28,31,32,35,36,39,40,43,44,47,48,51,52,55,56,58,59,62,63
  };
  int P2inxs[35]={
    1,2,3,7,8,9,10,14,15,16,17,22,23,24,25,29,30,31,32,37,38,39,40,45,46,47,48,53,54,55,56,60,61,62,63
  };
  int P3inxs[31]={
    4,5,6,7,8,9,10,18,19,20,21,22,23,24,25,33,34,35,36,37,38,39,40,49,50,51,52,53,54,55,56
  };
  int P4inxs[33]={
    11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
  };
  int P5inxs[33]={
    26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
  };
  int P6inxs[7]={
    57,58,59,60,61,62,63
  };
  static int P[7];
  int *Pinxs[7]={
    P0inxs, P1inxs, P2inxs, P3inxs, P4inxs, P5inxs, P6inxs
  }; 
  int PinxsSize[7]={
    35,35,35,31,33,33,7
  };

  //---------------------Get P0-P6-------------------
  int i;
  int j; 
  //unsigned long int data = strtoul(argv[1],NULL,16); 
  for(j = 0; j< LEN(P); j++){ 
    P[j] = (data>>Pinxs[j][0])%2; 
    for(i = 1; i<PinxsSize[j]; i++){
      P[j] = P[j] ^ ((data>>Pinxs[j][i])%2);
    }
  }
  //---------------------Get P7----------------------
  //XOR P7 with all the data bits
  P[7] = data%2;
  for(i=1; i<64; i++){
    P[7]=P[7] ^ ((data>>i)%2);
  }
  //XOR P7 with all the check bits
  for(i=0; i<7; i++){
    P[7]=P[7] ^ P[i];
  }
  
  //Transform P into a string
  stringstream ss; 
  for(int k: P)
    ss << k;
  //Transform P into an int
  int result; 
  result = stoi(ss.str().c_str(), NULL, 2); 
  
  return result;
  
  /*
  //Test 2: make bits come out exactly oposite each time
  eccCnt++; 
  if(eccCnt%2) return 0; 
  else return pow(2,8)-1;
  */
  
  /*Use below to check for correctness. 
  printf("These are the checkbits from P7-P0: ");
  for(i = 7; i>=0; i--){
    printf("%d", P[i]);
  }
  printf("\nCheckbits should be from P7-P0:     %s\n",argv[2]);
  */
}

/*Deprecated
//Get ECC bits for the entire 64byte data block. 
int* getECCbits(long long* data){
  int ECCbits[8]; 
  //Go through the 8byte words in data and get their ECC bits
  for(int i=0;i<8;i++){
    ECCbits[i]=getWordECC(data[i]);
  }
  
  return ECCbits; 
}
*/

//Get Average bitflips between two pairs of data
float getAvgDfs(unsigned long long** pair){
  unsigned long long c; 
  int j=0; 
  float avg=0; 
  float cnt=0;
  int numIters=sizeof(unsigned long long)*8;
  int numWords=8; 
  //char cStr[sizeof(unsigned long long)*8+1]; 
  for(int i=0;i<numWords;i++){
    c = pair[0][i]^pair[1][i];
    //Count number of 1's/bitflips in c
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) != 0){
	cnt++; 
      }
    }
    avg+=cnt/numIters;
    cnt=0; 
  }
  return avg/=(numWords); 
}

//Get Average bitflips between two pairs of ECC
float getAvgECCfs(int** pair){
  unsigned int c; 
  int j=0; 
  float avg=0; 
  float cnt=0;
  int numIters=8;
  int numWords=8; 
  //char cStr[sizeof(unsigned long long)*8+1]; 
  for(int i=0;i<numWords;i++){
    c = pair[0][i]^pair[1][i];
    //printf("Pairs and c: %#x, %#x, %#x\n",pair[0][i],pair[1][i],c);
    //Count number of 1's/bitflips in c
    for(int j=0; j<numIters; j++){
      if( (c & (1ULL << j)) != 0){
	cnt++; 
      }
    }
    avg+=cnt/numIters; 
    cnt = 0; 
  }
  return avg/=(numWords); 
}


int main(int argc, char* argv[]){
  
  DRAMtrace.open(argv[1]);
  //Initialize structures to keep our data and ECC
  unsigned long long* dataPair[2]; 
  float totDataAvg=0; 
  float totECCAvg=0; 
  int pairCnt = 0; 
  unsigned long long data1[8];  
  unsigned long long data2[8];
  int* ECCbitsPair[2];
  int ECC1[8]; 
  int ECC2[8]; 
  //int* isFirstPair; 
  ECCbitsPair[0]=ECC1; 
  ECCbitsPair[1]=ECC2; 
  dataPair[0]=data1; 
  dataPair[1]=data2; 
  
  
  //Get a pair of data to compare
  while(getDataPair(dataPair)){  
    //Get ECC bits
    for(int i=0;i<8;i++){
      ECC1[i]=getWordECC(dataPair[0][i]); 
      ECC2[i]=getWordECC(dataPair[1][i]);
      /*
      for(int k=0;k<8;k++){
	printf("ECC1: %x ECC2: %x\n", ECC1[k], ECC2[k]);
      } 
      */
    }
    //The first Data pair includes all 0's and the first write to address, ignore it
    if(isFirstPair) {
      //Do nothing
    }
    
    else{
      //Get data pair average bitflips (We use void pointer to use the same function for both dataPair and ECCbitsPair)
      totDataAvg += getAvgDfs(dataPair); 
      //Get ECC bit pair average bitflips
      totECCAvg += getAvgECCfs(ECCbitsPair); 
      pairCnt++; 
    }
  }
  
  totDataAvg/=pairCnt; 
  totECCAvg /=pairCnt;
  
  printf("Ratio of data bit flips: %lf\n", totDataAvg); 
  printf("Ratio of ECC bit  flips: %lf\n", totECCAvg);
  printf("Number of pairs counted: %d\n", pairCnt); 
 
  return 0; 
}
